<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificationEmail extends Mailable
{
    use Queueable, SerializesModels;
    
    //Title of the Notification
    public $title;

    //The Message 
    public $message;

    //Name of the User
    public $voter_name = "";

    //Voter Email
    public $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($voter_name, $title, $message, $email) {
        $this->voter_name = $voter_name;
        $this->message = $message;
        $this->title = $title;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject($this->title . " for " . $this->voter_name)
                    ->markdown('emails.NotificationEmail');
    }
}
