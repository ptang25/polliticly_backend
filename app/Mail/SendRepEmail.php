<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendRepEmail extends Mailable
{
    use Queueable, SerializesModels;

     //Voter's Email
     public $voter_email;

     //Voter's NAme
     public $voter_name;

     //Name of Representative
     public $name;

     //Email of Rep
     public $email;

     //The Message to send
     public $message;

     //URL
     public $url = "https://www.polliticly.com";
     
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($voter_email, $voter_name, $name, $email, $message)
    {
        $this->voter_email = $voter_email;
        $this->voter_name = $voter_name;
        $this->name = $name;
        $this->message = $message;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = env('MAIL_USERNAME');

        return $this->from($email, $this->voter_name)
                    ->replyTo($this->email, $this->voter_name)
                    ->subject('Constituent message from ' . $this->voter_name)
                    ->markdown('emails.sendEmail');
    }
}
