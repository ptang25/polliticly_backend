<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ConfirmMailSent extends Mailable
{
    use Queueable, SerializesModels;

    //Voter's NAme
    public $voter_name;

    //Representative Roles
    public $elected_officials_types = "";

    //The Message to send
    public $message;

    //URL
    public $url = "https://www.polliticly.com";
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($voter_name, $message, $officials)
    {
        $this->voter_name = $voter_name;
        $this->message = $message;

        //Convert Array into String
        $this->elected_officials_types = implode(", ", $officials);

        //Do some String Replacement
        $this->elected_officials_types = str_replace("assemblyDistrict", "State Assembly Member", $this->elected_officials_types);
        $this->elected_officials_types = str_replace("stateSenatorialDistrict", "State Senator", $this->elected_officials_types);
        $this->elected_officials_types = str_replace("cityCouncilDistrict", "City Council Member", $this->elected_officials_types);
        $this->elected_officials_types = str_replace("congressionalDistrict", "Congress Member", $this->elected_officials_types);
        $this->elected_officials_types = str_replace("communityDistrict", "Community Board", $this->elected_officials_types);


    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Confirmation of Emails sent')
                    ->markdown('emails.confirmEmailSent');
    }
}
