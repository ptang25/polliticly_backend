<?php

namespace App\Http\Controllers;

use App\Mail\ConfirmMailSent;
use App\Mail\SendRepEmail;
use App\Mail\SendDebateQuestion;
use App\Models\audit_trail;
use App\Models\district;
use App\Models\district_rep;
use App\Models\voters;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class DistrictController extends Controller
{

    /**
     * This Function is meant for the Admininistrator/Moderator access for Polliticly's backend.
     * 
     * Calling this function show the user all the districts details
     * 
     * Inside Request contains the Title question for the poll as well as an array of selected answers.
     */    
    public function index() {

        $assemblyRole = 'assemblyDistrict';
        $senateRole = 'stateSenatorialDistrict';
        $cityCouncilRole = 'cityCouncilDistrict';
        $congressionalRole = 'congressionalDistrict';
        $communityRole = 'communityDistrict';

        $assembly_reps = DB::table('district_rep')
                        ->leftjoin('districts', 'district_rep.districtID', '=', 'districts.id')
                        ->leftjoin('voter_districts', 'district_rep.districtID', '=', 'voter_districts.districtID')
                        ->where('districts.district_type', $assemblyRole)
                        ->select('district_rep.id', 'district_rep.rep_name', 'district_rep.rep_email', 'district_rep.rep_details', 'districts.district_num')
                        ->orderBy('districts.district_num')
                        ->distinct('district_rep.rep_name')
                        ->get();

        $senate_reps  = DB::table('district_rep')
                        ->leftjoin('districts', 'district_rep.districtID', '=', 'districts.id')
                        ->leftjoin('voter_districts', 'district_rep.districtID', '=', 'voter_districts.districtID')
                        ->where('districts.district_type', $senateRole)
                        ->select('district_rep.id', 'district_rep.rep_name', 'district_rep.rep_email', 'district_rep.rep_details', 'districts.district_num')
                        ->orderBy('districts.district_num')
                        ->distinct('district_rep.rep_name')
                        ->get();  

        $cc_reps    = DB::table('district_rep')
                        ->leftjoin('districts', 'district_rep.districtID', '=', 'districts.id')
                        ->leftjoin('voter_districts', 'district_rep.districtID', '=', 'voter_districts.districtID')
                        ->where('districts.district_type', $cityCouncilRole)
                        ->select('district_rep.id', 'district_rep.rep_name', 'district_rep.rep_email', 'district_rep.rep_details', 'districts.district_num')
                        ->orderBy('districts.district_num')
                        ->distinct('district_rep.rep_name')
                        ->get();

        $congress_reps = DB::table('district_rep')
                        ->leftjoin('districts', 'district_rep.districtID', '=', 'districts.id')
                        ->leftjoin('voter_districts', 'district_rep.districtID', '=', 'voter_districts.districtID')
                        ->where('districts.district_type', $congressionalRole )
                        ->select('district_rep.id', 'district_rep.rep_name', 'district_rep.rep_email', 'district_rep.rep_details', 'districts.district_num')
                        ->orderBy('districts.district_num')
                        ->distinct('district_rep.rep_name')
                        ->get();

        $community_reps = DB::table('district_rep')
                            ->leftjoin('districts', 'district_rep.districtID', '=', 'districts.id')
                            ->leftjoin('voter_districts', 'district_rep.districtID', '=', 'voter_districts.districtID')
                            ->where('districts.district_type', $communityRole)
                            ->select('district_rep.id', 'district_rep.rep_name', 'district_rep.rep_email', 'district_rep.rep_details', 'districts.district_num')
                            ->orderBy('districts.district_num')
                            ->distinct('district_rep.rep_name')
                            ->get();
        
        error_log("Count : " . count($community_reps));

        return view('reps', ['assembly' => $assembly_reps, 'senate' => $senate_reps, 'citycouncil' => $cc_reps, 'congress' => $congress_reps, 'community' => $community_reps]);
    }

    /**
     * This Function is meant for the Admininistrator/Moderator access for Polliticly's backend.
     * 
     * Calling this function show the View where a user can add a new Representative
     * 
     * Inside Request contains the Title question for the poll as well as an array of selected answers.
     */    
    public function addRep() {

        return view("create_rep");
    }

    /**
     * This Function is meant for the Admininistrator/Moderator access for Polliticly's backend.
     * 
     * Calling this function 
     * 
     * Inside Request contains the Title question for the poll as well as an array of selected answers.
     */    
    public function editRep($repID) {

        //Fetch the Rep
        $rep = DB::table('district_rep')
                    ->leftjoin('districts', 'district_rep.districtID', '=', 'districts.id')
                    ->where('district_rep.id', $repID)                        
                    ->select('district_rep.id', 'district_rep.rep_name', 'district_rep.rep_email', 'district_rep.rep_details', 'districts.district_num' , 'districts.district_type')
                    ->orderBy('districts.district_num')
                    ->distinct('district_rep.rep_name')
                    ->first();
        
        return view("create_rep", ['member' => $rep]);
    }

    /**
     * This Function is meant for the Admininistrator/Moderator access for Polliticly's backend.
     * 
     * Calling this function will store the Representative into the Database
     * 
     * Inside Request contains the Title question for the poll as well as an array of selected answers.
     */    
    public function createRep(Request $request) {

        $repID        = $request->input('repID');
        $repType      = $request->input('type');
        $districtNum  = $request->input('district_num');
        $name         = $request->input('name');
        $email        = $request->input('email');
        $details      = $request->input('details');

        //Update Rep
        if (isset($repID)) {

            //Check to see if the District Number for this type of Representative exists
            $district = district::where([['district_num', '=', $districtNum]])
                                        ->where([['district_type', '=', $repType]])
                                        ->first();

            //Record doesn't exist
            if (!isset($district->id)) {
                $district = new district();
                $district->district_num = $districtNum;
                $district->district_type = $repType;
                $district->save();

                //Audit Trail
                audit_trail::create(['operation' => 'create', 'table' => 'districts', 'field' => 'district_num', 'new_value' => $districtNum]);
                audit_trail::create(['operation' => 'create', 'table' => 'districts', 'field' => 'district_type', 'new_value' => $repType]);
            }

            //Update the Representative
            $rep = district_rep::find($repID);
            
            //Audit Trail
            //Update only if it is different
            if ($name != $rep->rep_name) {
                audit_trail::create(['operation' => 'update', 'table' => 'district_rep', 'field' => 'rep_name', 'old_value' =>  $rep->rep_name, 'new_value' => $name]);
                $rep->rep_name = $name;
            }

            //Update only if it is different
            if ($email != $rep->rep_email) {
                audit_trail::create(['operation' => 'update', 'table' => 'district_rep', 'field' => 'rep_email', 'old_value' =>  $rep->rep_email, 'new_value' => $email]);
                $rep->rep_email = $email;
            }

            //Update only if it is different
            if ($details != $rep->rep_details) {
                audit_trail::create(['operation' => 'update', 'table' => 'district_rep', 'field' => 'rep_details', 'old_value' =>  $rep->rep_details, 'new_value' => $details]);
                $rep->rep_details = $details;
            }

            //Update only if it is different
            if ($rep->districtID != $district->id) {
                audit_trail::create(['operation' => 'update', 'table' => 'district_rep', 'field' => 'districtID', 'old_value' =>  $rep->districtID, 'new_value' => $district->id]);
                $rep->districtID = $district->id;
            }

            $rep->save();            
        } 
        
        //Create a new Rep
        else {

            //Check to see if the District Number for this type of Representative exists
            $district = district::where([['district_num', '=', $districtNum]])
                                        ->where([['district_type', '=', $repType]])
                                        ->get();

            //Record doesn't exist
            if (!isset($district->id)) {
                $district = new district();
                $district->district_num = $districtNum;
                $district->district_type = $repType;
                $district->save();

                //Audit Trail
                audit_trail::create(['operation' => 'create', 'table' => 'districts', 'field' => 'district_num', 'new_value' => $districtNum]);
                audit_trail::create(['operation' => 'create', 'table' => 'districts', 'field' => 'district_type', 'new_value' => $repType]);
            }

            //Save the Representative
            $rep = new district_rep();
            $rep->rep_name = $name;
            $rep->rep_email = $email;
            $rep->rep_details = $details;
            $rep->districtID = $district->id;
            $rep->save();

            //Audit Trail
            audit_trail::create(['operation' => 'create', 'table' => 'district_rep', 'field' => 'rep_name', 'new_value' => $name]);
            audit_trail::create(['operation' => 'create', 'table' => 'district_rep', 'field' => 'rep_email', 'new_value' => $email]);
            audit_trail::create(['operation' => 'create', 'table' => 'district_rep', 'field' => 'rep_details', 'new_value' => $details]);
            audit_trail::create(['operation' => 'create', 'table' => 'district_rep', 'field' => 'districtID', 'new_value' => $district->id]);

        }

        return $this->index();
    }

    /**
     * This Function is meant for the Admininistrator/Moderator access for Polliticly's backend.
     * 
     * Calling this function gives the user an opportunity to delete a Representative
     * 
     * Inside Request contains the Title question for the poll as well as an array of selected answers.
     */
    public function removeRep($repID) {


        //Delete Poll
        $rep  = district_rep::find($repID);

        //Audit Trail
        audit_trail::create(['operation' => 'delete', 'table' => 'district_reps', 'field' => 'id', 'old_value' => $rep->id]);
        audit_trail::create(['operation' => 'delete', 'table' => 'district_reps', 'field' => 'rep_name', 'old_value' => $rep->rep_name]);
        audit_trail::create(['operation' => 'delete', 'table' => 'district_reps', 'field' => 'rep_email', 'old_value' => $rep->rep_email]);
        audit_trail::create(['operation' => 'delete', 'table' => 'district_reps', 'field' => 'rep_details', 'old_value' => $rep->rep_details]);
        audit_trail::create(['operation' => 'delete', 'table' => 'district_reps', 'field' => 'districtID', 'old_value' => $rep->districtID]);
        audit_trail::create(['operation' => 'delete', 'table' => 'district_reps', 'field' => 'borough', 'old_value' => $rep->borough]);
        
        //Delete the Rep
        $rep->delete();

        //Return the View showing all the Reps
        return $this->index();

    }


    /*****
     * This function pulls the appropriate emails to send the user's message to
     * 
     */
    public function sendEmailToReps (Request $request) {

        //Read in the JSON object to get the Voter ID
        $json = $request->json()->all();

        $voterID               = $json["userID"];
        $district_member_roles = $json["members"];
        $message               = $json["message"];

        $voter = voters::find($voterID);

        //$test_delivery_email = "peter.tang.lai@gmail.com";
        //$test_delivery_email = "nick@polliticly.com";

        //Save the Options
        foreach($district_member_roles as $member_role) {

            error_log($member_role);

            if ($member_role == "Debate Admin") {

                $email = "nick@polliticly.com";
                //$email = "peter.tang.lai@gmail.com";

                Mail::to($email)->send(new SendDebateQuestion($voter->email, $voter->first_name . " " . $voter->last_name, $email, $message));
            }

            else {
                $reps = DB::table('district_rep')
                        ->leftjoin('districts', 'district_rep.districtID', '=', 'districts.id')
                        ->leftjoin('voter_districts', 'district_rep.districtID', '=', 'voter_districts.districtID')
                        ->where('voter_districts.voterID', $voterID)
                        ->where('districts.district_type', $member_role)
                        ->select('district_rep.*')
                        ->get();

                error_log($reps);

                //Printout the emails
                foreach($reps as $rep) {

                    //Send the email
                    if ( (isset($rep->rep_email)) && (strlen($rep->rep_email) > 0) ) {
                
                        //Send Email to Elected Official
                        //Mail::to($test_delivery_email)->send(new SendRepEmail($voter->email, $voter->first_name . " " . $voter->last_name, $rep->rep_name, $rep->rep_email, $message));
                       
                        Mail::to($rep->rep_email)->send(new SendRepEmail($voter->email, $voter->first_name . " " . $voter->last_name, $rep->rep_name, $rep->rep_email, $message));
                    }
                }
            }
        }

        //Send Confirmation Email to user
        //Mail::to($test_delivery_email)->send(new ConfirmMailSent($voter->first_name . " " . $voter->last_name, $message, $district_member_roles));
        Mail::to($voter->email)->send(new ConfirmMailSent($voter->first_name . " " . $voter->last_name, $message, $district_member_roles));

        $data['code'] = 1;
        $data['msg']  = 'Emails sent';
        $data['data'] = '';

        return response()->json($data);   
    }

}
