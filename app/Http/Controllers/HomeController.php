<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //The ID of the user on the PHP Admin System
        $user_access = Auth::user()->access;

        //User that has been granted access manually
        if ($user_access == 1) {
            return view('home');
        }
        else {
            return view('waiting');
        }
    }
}
