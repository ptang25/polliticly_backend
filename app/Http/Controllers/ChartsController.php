<?php

namespace App\Http\Controllers;

use App\Models\district;
use App\Models\poll;
use App\Models\poll_choices;
use App\Models\vote_selections;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Khill\Lavacharts\Laravel\LavachartsFacade;

class ChartsController extends Controller
{
    /**
     * Test Charts
     */    
    public function testChart($hash, $type, $num) {

        //Find Poll
        $poll = poll::where([['hashcode', '=', $hash]])->first();
        
        if ($poll) {
            //Get Choices
            $poll_choice = poll_choices::where([['pollID', '=', $poll->id]])->get();

            //District Information
            $district_type = "";

            //This variable is used to display the Type of district on the HTML
            $district_type_display = "";

            switch ($type) {
                case "city": 
                    $district_type = "city";
                    $district_type_display = "City";
                    break;
                case "b":
                    $district_type = "boroughs";
                    $district_type_display = "Boroughs";
                    break;
                case "a":
                    $district_type = "assemblyDistrict";
                    $district_type_display = "Assembly";
                    break;
                case "c":
                    $district_type = "congressionalDistrict";
                    $district_type_display = "Congress";
                    break;
                case "s":
                    $district_type = "stateSenatorialDistrict";
                    $district_type_display = "Senator";
                    break;
                case "cm":
                    $district_type = "communityDistrict";
                    $district_type_display = "Community";
                    break;
                case "cc":
                    $district_type = "cityCouncilDistrict";
                    $district_type_display = "City Council";
                    break;
                default:
                    echo "";
            }

            if ($district_type_display == "City") {

                //Get the results of the Poll
                $overall_selections = DB::table('vote_selections')
                    ->join('poll_choices', 'poll_choices.id', '=', 'vote_selections.selection')
                    ->selectRaw('count(*) as total, poll_choices.content')
                    ->where('vote_selections.pollID', $poll->id)
                    ->groupBy('vote_selections.selection', 'poll_choices.content')
                    ->get();

                $this->selectionsGraph($overall_selections);

                //Get the Results based on this district, with filters to Age, Ethnicity and Gender
                $selections = DB::table('vote_selections')
                            ->join('voters', 'voters.id', '=', 'vote_selections.voterID')
                            ->join('poll_choices', 'poll_choices.id', '=', 'vote_selections.selection')
                            ->where('vote_selections.pollID', $poll->id)
                            ->select('poll_choices.content', 'voters.birthday', 'voters.ethnicity', 'voters.gender')
                            ->get();

                $this->voteInCity($selections, $poll_choice);

                $age_to_choices = $this->ageGraph($selections, $poll_choice);
                $this->agePieChart($age_to_choices, count($poll_choice));
                    
                $gender_to_choices = $this->genderGraph($selections, $poll_choice);
                $this->genderPieChart($gender_to_choices, count($poll_choice));

                $ethnicity_to_choices = $this->ethnicityGraph($selections, $poll_choice);
                $this->ethnicityPieChart($ethnicity_to_choices, count($poll_choice));

                return view('charts', ['question' => $poll->question, 'district_type' => $district_type_display, 'district_num' => $num]);

            }
            else if ($district_type_display == "Boroughs") {

                //Get the results of the Poll
                $overall_selections = DB::table('vote_selections')
                    ->join('poll_choices', 'poll_choices.id', '=', 'vote_selections.selection')
                    ->selectRaw('count(*) as total, poll_choices.content')
                    ->where('vote_selections.pollID', $poll->id)
                    ->groupBy('vote_selections.selection', 'poll_choices.content')
                    ->get();

                $this->selectionsGraph($overall_selections);

                //Get the Borough Name
                $borough = urldecode($num);

                $districtsCovered = DB::table('district_boroughs_maps')
                                ->join('boroughs', 'boroughs.id', '=', 'district_boroughs_maps.boroughID')
                                ->where('boroughs.boroughName', $borough)
                                ->select('district_boroughs_maps.districtID')
                                ->get();

                $districts = $districtsCovered->pluck(["districtID"])->all();
                
                //Get the Results based on this district, with filters to Age, Ethnicity and Gender
                $selections = DB::table('vote_selections')
                            ->join('voters', 'voters.id', '=', 'vote_selections.voterID')
                            ->join('poll_choices', 'poll_choices.id', '=', 'vote_selections.selection')
                            ->join('voter_districts', 'voters.id', '=', 'voter_districts.voterID')
                            ->where('vote_selections.pollID', $poll->id)
                            ->whereIn('voter_districts.districtID', $districts)
                            ->select('poll_choices.content', 'voters.birthday', 'voters.ethnicity', 'voters.gender')
                            ->get();

                $this->voteInBorough($selections, $poll_choice);

                $age_to_choices = $this->ageGraph($selections, $poll_choice);

                $this->agePieChart($age_to_choices, count($poll_choice));
                                
                $gender_to_choices = $this->genderGraph($selections, $poll_choice);
                $this->genderPieChart($gender_to_choices, count($poll_choice));
            
                $ethnicity_to_choices = $this->ethnicityGraph($selections, $poll_choice);
                $this->ethnicityPieChart($ethnicity_to_choices, count($poll_choice));
                
                return view('charts', ['question' => $poll->question, 'district_type' => $district_type_display, 'district_num' => $num]);
            }

            else {

                //District
                $district = district::where([['district_type', '=', $district_type], ['district_num', '=', $num]])->first();

                if (isset($district)) {

                    //Get the results of the Poll
                    $overall_selections = DB::table('vote_selections')
                                    ->join('poll_choices', 'poll_choices.id', '=', 'vote_selections.selection')
                                    ->selectRaw('count(*) as total, poll_choices.content')
                                    ->where('vote_selections.pollID', $poll->id)
                                    ->groupBy('vote_selections.selection', 'poll_choices.content')
                                    ->get();

                    if (isset($overall_selections)) {
                        $this->selectionsGraph($overall_selections);
                    }

                    //Get the Results based on this district, with filters to Age, Ethnicity and Gender
                    $selections = DB::table('vote_selections')
                                ->join('voters', 'voters.id', '=', 'vote_selections.voterID')
                                ->join('poll_choices', 'poll_choices.id', '=', 'vote_selections.selection')
                                ->join('voter_districts', 'voters.id', '=', 'voter_districts.voterID')
                                ->where('vote_selections.pollID', $poll->id)
                                ->where('voter_districts.districtID', $district->id)
                                ->select('poll_choices.content', 'voters.birthday', 'voters.ethnicity', 'voters.gender')
                                ->get();

                    if (isset($selections)) {

                        $this->votesInDistrict($selections, $poll_choice);

                        $age_to_choices = $this->ageGraph($selections, $poll_choice);
                        $this->agePieChart($age_to_choices, count($poll_choice));
                        
                        $gender_to_choices = $this->genderGraph($selections, $poll_choice);
                        $this->genderPieChart($gender_to_choices, count($poll_choice));

                        $ethnicity_to_choices = $this->ethnicityGraph($selections, $poll_choice);
                        $this->ethnicityPieChart($ethnicity_to_choices, count($poll_choice));
                        
                    }
                }

                return view('charts', ['question' => $poll->question, 'district_type' => $district_type_display, 'district_num' => $num]);
            }
        }

        else {
            $data['title'] = 'Error';
            $data['name'] = 'The Results Page you are request is invalid';
            
            return response()
            ->view('errors.404',$data,404);
        }
    }

    /**
     * This Function builds out the Bar Graph with the aggregrated results from all districts
     */
    private function selectionsGraph($poll_selections) {

        $votes  = \Lava::DataTable();

        $votes->addStringColumn('Votes')
                ->addNumberColumn('Votes');
        
        foreach ($poll_selections as $selection) {
         
            $votes->addRow([$selection->content, $selection->total]);
        }

        \Lava::ColumnChart('Votes', $votes, [
            'title' => 'Votes from All Districts',
            'titleTextStyle' => [
                'color'    => '#eb6b2c',
                'fontSize' => 14
            ],
            'hAxis' => [
                'slantedText' => false,
                'textPosition' => 'out'
            ]  
        ]);

    }

    /*-------------------------- Age Graphs ----------------------------*/
    /**
     * This Function build the Age vs. Choices Graph
     */
    private function ageGraph($selections, $poll_choice) {

        //Number of Age Groups
        $num_groups = 6;
        
        //Age Chart
        $poll_data = \Lava::DataTable();

        //Init Age Group
        for($index = 0; $index < $num_groups; $index++) {

            $age_groups[$index] = [];
        }

        $poll_data->addStringColumn('Choices');

        //Setup Columns
        $poll_data->addNumberColumn('18-24');
        $poll_data->addNumberColumn('25-34');
        $poll_data->addNumberColumn('35-44');
        $poll_data->addNumberColumn('45-54');
        $poll_data->addNumberColumn('55-64');
        $poll_data->addNumberColumn('65+');
        
        //Add Choices to Chart
        foreach ($poll_choice as $i => $choice) {

            $selection_indices[$choice->content] = $i;

        }

        //2D Array
        $choices_to_age_array = array_fill(0, count($selection_indices), array_fill(0, $num_groups, 0));
        
        //Data
        foreach($selections as $selection) {

            //Get the Age of the voter based on their birthday
            $age = date_diff(date_create($selection->birthday), date_create('now'))->y;

            //Get the index of the selection (1st choice, 2nd choice, etc.)
            $index = $selection_indices[$selection->content];

            if (($age > 18) && ($age < 25)) {
                $choices_to_age_array[$index][0] += 1;
            }
            else if (($age > 24) && ($age < 35)) {
                error_log("In Array : " . $choices_to_age_array[$index][1]);
                $choices_to_age_array[$index][1] += 1;
            }
            else if (($age > 34) && ($age < 45)) {
                $choices_to_age_array[$index][2] += 1;
            }
            else if (($age > 44) && ($age < 55)) {
                $choices_to_age_array[$index][3] += 1;
            }
            else if (($age > 54) && ($age < 65)) {
                $choices_to_age_array[$index][4] += 1;
            }
            else {
                $choices_to_age_array[$index][5] += 1;
            }
        }

        //The Rows
        $choices = [];

        //Place into Graph Data
        for ($x = 0; $x < count($poll_choice); $x++) {

            //Init
            $choices[$x] = [];
            
            //Add first element
            array_push($choices[$x], $poll_choice[$x]->content);

            for ($y = 0; $y < $num_groups; $y++) {

                array_push($choices[$x], $choices_to_age_array[$x][$y]);
            }

            $poll_data->addRow($choices[$x]);

        }

        //Build the Graph
        \Lava::ComboChart('Age_Selections', $poll_data, [
            'title' => 'Selections by Age',
            'titleTextStyle' => [
                'fontSize' => 40
            ],
            'vAxis' => [
                'title' => 'Frequency'],
            'hAxis' => [
                'title' => 'Choices',
                'slantedText' => false,
                'textPosition' => 'out',
                'showTextEvery' => 1,
            ],
            'seriesType' => 'bars',
            'legend' => [
                'position' => 'bottom'
            ]
        ]);

        return $choices_to_age_array;
    
    }

    /**
     * This Function build the Age vs. Choices Graph
     */
    private function agePieChart($choices_to_age_array, $num_choices) {

        $num_groups = 6;

        //////Create the Pie Chart with the data above
        $age_pie = \Lava::DataTable();

        $age_pie->addStringColumn('Age of Voters')
                ->addNumberColumn('Age Range');

        //Get the Age totals
        $age_count = [];

        //Place into Graph Data
        for ($x = 0; $x < $num_groups; $x++) {

            //Initialize
            $age_count[$x] = 0;       
            
            for ($y = 0; $y < $num_choices; $y++) {

                $age_count[$x] += $choices_to_age_array[$y][$x];
            }
        }

        $age_pie->addRow(['18-24', $age_count[0]]);
        $age_pie->addRow(['25-34', $age_count[1]]);
        $age_pie->addRow(['35-44', $age_count[2]]);
        $age_pie->addRow(['45-54', $age_count[3]]);
        $age_pie->addRow(['55-64', $age_count[4]]);
        $age_pie->addRow(['65+', $age_count[5]]);

        \Lava::PieChart('Age', $age_pie, [
            'title'  => 'Age Range of Voters',
            'is3D'   => true,
            'slices' => [
                ['offset' => 0.2],
                ['offset' => 0.25],
                ['offset' => 0.3]
            ],
            'hAxis' => [
                'slantedText' => false,
                'textPosition' => 'out',
                'showTextEvery' => 1,
            ]  
        ]);

    }

    /*-------------------------- End Age Graphs ----------------------------*/

    /*-------------------------- Gender Graphs ----------------------------*/
    /**
     * This Function builds the Gender vs. Choices Graph
     */
    private function genderGraph($selections, $poll_choice) {

        //Number of Age Groups
        $num_groups = 5;

        //Age Chart
        $poll_data = \Lava::DataTable();

        //Init Gender Group
        for($index = 0; $index < $num_groups; $index++) {

            $gender_groups[$index] = [];
        }

        $poll_data->addStringColumn('Choices');

        //Setup Columns
        $poll_data->addNumberColumn('Male');
        $poll_data->addNumberColumn('Female');
        $poll_data->addNumberColumn('Non-Binary');
        $poll_data->addNumberColumn('Other');
        $poll_data->addNumberColumn('Prefer Not to Say');

        //Add Choices to Chart
        foreach ($poll_choice as $i => $choice) {

            $selection_indices[$choice->content] = $i;

        }

        //2D Array
        $choices_to_gender_array = array_fill(0, count($selection_indices), array_fill(0, $num_groups, 0));
        
        //Data
        foreach($selections as $selection) {

            //Get the gender of the voter
            $gender = $selection->gender;

            //Get the index of the selection (1st choice, 2nd choice, etc.)
            $index = $selection_indices[$selection->content];

            if ($gender == 'Male') {
                $choices_to_gender_array[$index][0] += 1;
            }
            else if ($gender == 'Female') {
                $choices_to_gender_array[$index][1] += 1;
            }
            else if ($gender == 'Non-Binary') {
                $choices_to_gender_array[$index][2] += 1;
            }
            else if ($gender == 'Other') {
                $choices_to_gender_array[$index][3] += 1;
            }
            else {
                $choices_to_gender_array[$index][4] += 1;
            }
        }

        //The Rows
        $choices = [];

        //Place into Graph Data
        for ($x = 0; $x < count($poll_choice); $x++) {

            //Init
            $choices[$x] = [];
            
            //Add first element
            array_push($choices[$x], $poll_choice[$x]->content);

            for ($y = 0; $y < $num_groups; $y++) {

                array_push($choices[$x], $choices_to_gender_array[$x][$y]);
            }

            $poll_data->addRow($choices[$x]);

        }

        //Build the Graph
        \Lava::ComboChart('Gender_Selections', $poll_data, [
            'title' => 'Selections by Gender',
            'titleTextStyle' => [
                'fontSize' => 40
            ],
            'vAxis' => [
                'title' => 'Frequency'],
            'hAxis' => [
                'title' => 'Choices',
                'slantedText' => false,
                'textPosition' => 'out',
                'showTextEvery' => 1,
            ],
            'seriesType' => 'bars',
            'legend' => [
                'position' => 'bottom'
            ]
        ]);

        return $choices_to_gender_array;
    }

    /**
     * This Function build the Genders Pie Chart
     */
    private function genderPieChart($choices_to_gender_array, $num_choices) {

        $num_groups = 5;

        //////Create the Pie Chart with the data above
        $gender_pie = \Lava::DataTable();

        $gender_pie->addStringColumn('Gender of Voters')
                ->addNumberColumn('Genders');

        //Get the Age totals
        $gender_count = [];

        //Place into Graph Data
        for ($x = 0; $x < $num_groups; $x++) {

            //Initialize
            $gender_count[$x] = 0;       
            
            for ($y = 0; $y < $num_choices; $y++) {

                $gender_count[$x] += $choices_to_gender_array[$y][$x];
            }
        }

        $gender_pie->addRow(['Male', $gender_count[0]]);
        $gender_pie->addRow(['Female', $gender_count[1]]);
        $gender_pie->addRow(['Non-Binary', $gender_count[2]]);
        $gender_pie->addRow(['Other', $gender_count[3]]);
        $gender_pie->addRow(['Prefer Not to Sat', $gender_count[4]]);

        \Lava::PieChart('Gender', $gender_pie, [
            'title'  => 'Gender of Voters',
            'is3D'   => true,
            'slices' => [
                ['offset' => 0.2],
                ['offset' => 0.25],
                ['offset' => 0.3]
            ],
            'hAxis' => [
                'slantedText' => false,
                'textPosition' => 'out',
                'showTextEvery' => 1,
            ]  
        ]);

    }

    /*-------------------------- End Gender Graphs ----------------------------*/

    /*-------------------------- Ethnicity Graphs ----------------------------*/
    /**
     * This Function builds the Gender vs. Choices Graph
     */
    private function ethnicityGraph($selections, $poll_choice) {

        //Number of Ethnicity Groups
        //["Asian / Pacific Islander", "Black or African American", "Hispanic or Latino", "Native American or American Indian", "White", "Other", "Prefer Not to Say"]
        $num_groups = 7;

        //Age Chart
        $poll_data = \Lava::DataTable();

        //Init Ethnicity Group
        for($index = 0; $index < $num_groups; $index++) {

            $gender_groups[$index] = [];
        }

        $poll_data->addStringColumn('Choices');

        //Setup Columns
        $poll_data->addNumberColumn('Asian / Pacific Islander');
        $poll_data->addNumberColumn('Black or African American');
        $poll_data->addNumberColumn('Hispanic or Latino');
        $poll_data->addNumberColumn('Native American or American Indian');
        $poll_data->addNumberColumn('White');
        $poll_data->addNumberColumn('Other');
        $poll_data->addNumberColumn('Prefer Not to Say');

        //Add Choices to Chart
        foreach ($poll_choice as $i => $choice) {
            $selection_indices[$choice->content] = $i;
        }

        //2D Array
        $choices_to_ethnicity_array = array_fill(0, count($selection_indices), array_fill(0, $num_groups, 0));
        
        //Data
        foreach($selections as $selection) {

            //Get the gender of the voter
            $ethnicity = $selection->ethnicity;

            //Get the index of the selection (1st choice, 2nd choice, etc.)
            $index = $selection_indices[$selection->content];

            if ($ethnicity == 'Asian / Pacific Islander') {
                $choices_to_ethnicity_array[$index][0] += 1;
            }
            else if ($ethnicity == 'Black or African American') {
                $choices_to_ethnicity_array[$index][1] += 1;
            }
            else if ($ethnicity == 'Hispanic or Latino') {
                $choices_to_ethnicity_array[$index][2] += 1;
            }
            else if ($ethnicity == 'Native American or American Indian') {
                $choices_to_ethnicity_array[$index][3] += 1;
            }
            else if ($ethnicity == 'White') {
                $choices_to_ethnicity_array[$index][4] += 1;
            }
            else if ($ethnicity == 'Other') {
                $choices_to_ethnicity_array[$index][5] += 1;
            }
            else {
                $choices_to_ethnicity_array[$index][6] += 1;
            }
        }

        //The Rows
        $choices = [];

        //Place into Graph Data
        for ($x = 0; $x < count($poll_choice); $x++) {

            //Init
            $choices[$x] = [];
            
            //Add first element
            array_push($choices[$x], $poll_choice[$x]->content);

            for ($y = 0; $y < $num_groups; $y++) {

                array_push($choices[$x], $choices_to_ethnicity_array[$x][$y]);
            }

            $poll_data->addRow($choices[$x]);

        }

        //Build the Graph
        \Lava::ComboChart('Ethnicity_Selections', $poll_data, [
            'title' => 'Selections by Ethnicity',
            'titleTextStyle' => [
                'fontSize' => 40
            ],
            'vAxis' => [
                'title' => 'Frequency'],
            'hAxis' => [
                'title' => 'Choices',
                'slantedText' => false,
                'textPosition' => 'out',
                'showTextEvery' => 1,
            ],
            'seriesType' => 'bars',
            'legend' => [
                'position' => 'bottom'
            ]
        ]);

        return $choices_to_ethnicity_array;
    }

    /**
     * This Function build the Ethnicity Pie Chart
     */
    private function ethnicityPieChart($choices_to_ethniticy_array, $num_choices) {

        $num_groups = 7;

        //////Create the Pie Chart with the data above
        $ethnicity_pie = \Lava::DataTable();

        $ethnicity_pie->addStringColumn('Ethniticy of Voters')
                ->addNumberColumn('Ethnicities');

        //Get the Age totals
        $ethnicity_count = [];

        //Place into Graph Data
        for ($x = 0; $x < $num_groups; $x++) {

            //Initialize
            $ethnicity_count[$x] = 0;       
            
            for ($y = 0; $y < $num_choices; $y++) {

                $ethnicity_count[$x] += $choices_to_ethniticy_array[$y][$x];
            }
        }

        $ethnicity_pie->addRow(['Asian / Pacific Islander', $ethnicity_count[0]]);
        $ethnicity_pie->addRow(['Black or African American', $ethnicity_count[1]]);
        $ethnicity_pie->addRow(['Hispanic or Latino', $ethnicity_count[2]]);
        $ethnicity_pie->addRow(['Native American or American Indian', $ethnicity_count[3]]);
        $ethnicity_pie->addRow(['White', $ethnicity_count[4]]);
        $ethnicity_pie->addRow(['Other', $ethnicity_count[5]]);
        $ethnicity_pie->addRow(['Prefer Not to Say', $ethnicity_count[6]]);

        \Lava::PieChart('Ethnicity', $ethnicity_pie, [
            'title'  => 'Ethnicity of Voters',
            'is3D'   => true,
            'slices' => [
                ['offset' => 0.2],
                ['offset' => 0.25],
                ['offset' => 0.3]
            ],
            'hAxis' => [
                'slantedText' => false,
                'textPosition' => 'out',
                'showTextEvery' => 1,
            ]  
        ]);
    }

    /*-------------------------- End Ethnicity Graphs ----------------------------*/

    /*-------------------------- Vote from Voters in a specific District ----------------------------*/
    private function votesInDistrict($selections, $poll_choice) {

        //Chart
        $poll_data = \Lava::DataTable();

        $poll_data->addStringColumn('Votes')
                   ->addNumberColumn('Votes');

        //Add Choices to Chart
        foreach ($poll_choice as $i => $choice) {
            $selection_indices[$choice->content] = $i;
        }

        //Array to Store Results
        $results = array_fill(0, count($poll_choice), 0);

        //Go through our votes from our district
        foreach($selections as $selection) {

            $results[$selection_indices[$selection->content]] += 1;
        }

        //Lastly Add the rowData to Chart
        foreach($results as $i => $row) {
            $poll_data->addRow([$poll_choice[$i]->content,$row]);
        }

        \Lava::ColumnChart('DistrictVotes', $poll_data, [
            'title' => 'District Only Votes',
            'titleTextStyle' => [
                'fontSize' => 14
            ],
            'hAxis' => [
                'slantedText' => false,
                'textPosition' => 'out',
                'showTextEvery' => 1,
            ]  
        ]);
    }

    /*******************\
     * When the Admin selects results by entire City
     */
    private function voteInCity($selections, $poll_choice) {

        //Chart
        $poll_data = \Lava::DataTable();

        $poll_data->addStringColumn('Votes')
                   ->addNumberColumn('Votes');

        //Add Choices to Chart
        foreach ($poll_choice as $i => $choice) {
            $selection_indices[$choice->content] = $i;
        }

        //Array to Store Results
        $results = array_fill(0, count($poll_choice), 0);

        //Go through our votes from our district
        foreach($selections as $selection) {

            $results[$selection_indices[$selection->content]] += 1;
        }

        //Lastly Add the rowData to Chart
        foreach($results as $i => $row) {
            $poll_data->addRow([$poll_choice[$i]->content,$row]);
        }

        \Lava::ColumnChart('CityVotes', $poll_data, [
            'title' => 'City Wide Votes',
            'titleTextStyle' => [
                'fontSize' => 14
            ],
            'hAxis' => [
                'slantedText' => false,
                'textPosition' => 'out',
                'showTextEvery' => 1,
            ]  
        ]);
    }

     /*******************\
     * When the Admin selects results by seperate Boroughs
     */
    private function voteInBorough($selections, $poll_choice) {

        //Chart
        $poll_data = \Lava::DataTable();

        $poll_data->addStringColumn('Votes')
                   ->addNumberColumn('Votes');

        //Add Choices to Chart
        foreach ($poll_choice as $i => $choice) {
            $selection_indices[$choice->content] = $i;
        }

        //Array to Store Results
        $results = array_fill(0, count($poll_choice), 0);

        //Go through our votes from our district
        foreach($selections as $selection) {

            $results[$selection_indices[$selection->content]] += 1;
        }

        //Lastly Add the rowData to Chart
        foreach($results as $i => $row) {
            $poll_data->addRow([$poll_choice[$i]->content,$row]);
        }

        \Lava::ColumnChart('BoroughVotes', $poll_data, [
            'title' => 'Borough Votes',
            'titleTextStyle' => [
                'fontSize' => 14
            ],
            'hAxis' => [
                'slantedText' => false,
                'textPosition' => 'out',
                'showTextEvery' => 1,
            ]  
        ]);

    }
}
