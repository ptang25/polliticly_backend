<?php

namespace App\Http\Controllers;

use App\Charts\PollChart;
use App\Models\audit_trail;
use App\Models\boroughs;
use App\Models\district;
use App\Models\firebase_push_notifications;
use App\Models\poll;
use App\Models\poll_choices;
use App\Models\poll_comments;
use App\Models\Poll_Districts;
use App\Models\poll_likes;
use App\Models\vote_selections;
use App\Models\voters;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;

class PollController extends Controller
{

    /**
     * This Function just returns the Create a Poll View
     *
     */
    public function showManagePollsView() 
    {
        
        $polls = poll::where([['status', '=', 1]])->get();

        //Iterate through each poll to setup the labels and Values
        foreach ($polls as $poll) {

            $choices = poll_choices::where([['pollID', '=', $poll->id]])->get();
            $num_votes = vote_selections::where([['pollID', '=', $poll->id]])->get();

            //Empty Arrays to store the Text and their Percentages
            $poll_answers = [];
            $poll_percentages = [];

            //Choices from each Poll
            foreach ($choices as $choice) {

                //Put the Choice Text into the Array
                array_push($poll_answers, $choice->content);

                //Prevent Division by Zero
                if (count($num_votes) == 0) {
                    array_push($poll_percentages, 0);
                }
                else {
                    //Get the number that votes for this choice
                    $gross_votes = vote_selections::where([['selection', '=', $choice->id]])->get();
                    array_push($poll_percentages, count($gross_votes) / count($num_votes));
                }
            }

            //Add the two elements into the Poll variable to be displayed
            $poll->choices = $poll_answers;
            $poll->percentages = $poll_percentages;
        }

        return view('manage', ['polls' => $polls]);
    }

    /**
     * This Function just returns the View of all Polls waiting for approval
     *
     */
    public function approvePolls() 
    {
        
        $polls = poll::where([['status', '=', 0]])->get();

        //Iterate through each poll to setup the labels and Values
        foreach ($polls as $poll) {

            $choices = poll_choices::where([['pollID', '=', $poll->id]])->get();

            //Empty Arrays to store the Text and their Percentages
            $poll_answers = [];

            //Choices from each Poll
            foreach ($choices as $choice) {

                //Put the Choice Text into the Array
                array_push($poll_answers, $choice->content);
            }

            //Add the two elements into the Poll variable to be displayed
            $poll->choices = $poll_answers;
        }

        return view('other_polls', ['polls' => $polls, 'poll_type' => 'Polls waiting for approval']);
    }

    /**
     * This Function accepts the Poll
     *
     */
    public function acceptPoll($pollID) 
    {
        
        $poll = poll::find($pollID);

        //Set the Status of the Poll to be approved
        $poll->status = 1;
        $poll->save();

        //Set all districts in the city to be able to view the Polls
        $myDistricts = district::all();

        foreach($myDistricts as $myDistrict) {
                    
            $poll_district = new Poll_Districts();
            $poll_district->districtID = $myDistrict->id;
            $poll_district->pollID = $pollID;
            $poll_district->save();
        }

        //Audit Trail
        audit_trail::create(['operation' => 'update', 'table' => 'poll', 'field' => 'status', 'new_value' => 1]);

        return $this->approvePolls();
    }

    /**
     * This Function accepts the Poll
     *
     */
    public function rejectPoll($pollID) 
    {
        
        $poll = poll::find($pollID);

        //Set the Status of the Poll to be rejected
        $poll->status = 3;
        $poll->save();

        //Audit Trail
        audit_trail::create(['operation' => 'update', 'table' => 'poll', 'field' => 'status', 'new_value' => 3]);

        return $this->showManagePollsView();
    }

    /**
     * This Function just returns the View of all Polls that have been archived
     *
     */
    public function archivedPolls() 
    {
        
        $polls = poll::where([['status', '=', 2]])->get();

        //Iterate through each poll to setup the labels and Values
        foreach ($polls as $poll) {

            $choices = poll_choices::where([['pollID', '=', $poll->id]])->get();

            //Empty Arrays to store the Text and their Percentages
            $poll_answers = [];

            //Choices from each Poll
            foreach ($choices as $choice) {

                //Put the Choice Text into the Array
                array_push($poll_answers, $choice->content);
            }

            //Add the two elements into the Poll variable to be displayed
            $poll->choices = $poll_answers;
        }

        return view('other_polls', ['polls' => $polls, 'poll_type' => 'Archived Polls']);
    }

    /**
     * This Function just returns the View of all Polls that have been rejected
     *
     */
    public function rejectedPolls() 
    {
        
        $polls = poll::where([['status', '=', 3]])->get();

        //Iterate through each poll to setup the labels and Values
        foreach ($polls as $poll) {

            $choices = poll_choices::where([['pollID', '=', $poll->id]])->get();

            //Empty Arrays to store the Text and their Percentages
            $poll_answers = [];

            //Choices from each Poll
            foreach ($choices as $choice) {

                //Put the Choice Text into the Array
                array_push($poll_answers, $choice->content);
            }

            //Add the two elements into the Poll variable to be displayed
            $poll->choices = $poll_answers;
        }

        return view('other_polls', ['polls' => $polls, 'poll_type' => 'Rejected Polls']);
    }
    

    /**
     * This Function Archives a Poll
     *
     */
    public function archivePoll($pollID) 
    {
        
        $poll = poll::find($pollID);

        //Set the Status of the Poll to be archived
        $poll->status = 2;
        $poll->save();

        //Audit Trail
        audit_trail::create(['operation' => 'update', 'table' => 'poll', 'field' => 'status', 'new_value' => 2]);

        return $this->showManagePollsView();
    }
    
    /**
     * This Function just returns the Create a Poll View
     *
     */
    public function createPollsView() 
    {
        //Get the Districts
        $assembly_districts = $this->getDistricts(23,87,[]);
        $citycouncil_districts = $this->getDistricts(1,51,[]);
        $congress_districts = $this->getDistricts(3,16,[]);
        $senate_districts = $this->getDistricts(10,36,[]);

        //Merge the Community Boards from the suburbs
        $c_manhattan = $this->getDistricts(101,112,[]);
        $c_bronx = $this->getDistricts(201,212,[]);
        $c_queens = $this->getDistricts(401,414,[]);
        $c_statenisland = $this->getDistricts(501,503,[]);
        $c_brooklyn = $this->getDistricts(301,318,[]);
        $community_districts = array_merge($c_manhattan, $c_bronx, $c_brooklyn, $c_queens, $c_statenisland);

        //Get Borough Names
        $results = DB::table('district_boroughs_maps')
                        ->join('boroughs', 'district_boroughs_maps.boroughID', '=', 'boroughs.id')
                        ->select('boroughs.boroughName', 'boroughs.city')
                        ->distinct('boroughs.id')
                        ->get();
        
        //Extract Cities
        $cities = $results->pluck(["city"])->all();
        $cities = array_unique($cities);

        $boroughs = $results->pluck(["boroughName"])->all();
        $boroughs = array_unique($boroughs);

        return view('create_poll', ['assembly' => $assembly_districts, 'citycouncil' => $citycouncil_districts, 'community' => $community_districts, 'congress' => $congress_districts, 'senate' => $senate_districts, 'borough' => $boroughs, 'city' => $cities]);


    }

    /**
     * This Function is meant for the Admininistrator/Moderator access for Polliticly's backend.
     * 
     * Calling this function creates a Poll with it respective poll choices for Voters to choose from.
     * $request                          Blade Form file
     * 
     * Inside Request contains the Title question for the poll as well as an array of selected answers.
     */
    public function createPoll(Request $request) {

        $pollID        = $request->input('pollID');
        $poll_question = $request->input('poll_question');
        $choices_array = $request->input('poll_choices');
        $poll_status   = $request->input('status');
        $district_choices = $request->input('poll_districts');
        $sponsorTitle  = $request->input('poll_sponsor');
        $more_info     = $request->input('poll_more');
        $link          = $request->input('poll_link');

        //The ID of the user on the PHP Admin System
        $userID = Auth::user()->id;

        //Are we sending a Poll update?
        if (isset($pollID)) {

            $poll = poll::find($pollID);

            $poll_choices = poll_choices::where([['pollID', '=', $poll->id]])
                            ->orderby('id')    
                            ->get();

            //Update the Question    
            $poll->question = $poll_question;

            if (isset($sponsorTitle) && strlen($sponsorTitle) > 0) {
                $poll->sponsor = $sponsorTitle;
            }
            else {
                $poll->sponsor = "";
            }
            $poll->more = $more_info;
            $poll->link = $link;

            if (isset($poll_status) && ($poll_status == 'true')) {
                //Toggle the Status
                $poll->status   = 1;
            } else {
                $poll->status   = 0;
            }

            //Save the Options
            foreach($choices_array as $i => $choice) {

                //Validate that the Choice text for the poll isn't empty
                if (strlen($choice) > 0) {

                    //Are we assigning a new option?
                    if (isset($poll_choices[$i])) {

                        //Audit Trail
                        audit_trail::create(['operation' => 'update', 'table' => 'poll_choices', 'voterID' => $userID, 'field' => 'content', 'old_value' =>  $poll_choices[$i]->content,'new_value' => $choice]);

                        //Assign new Value
                        $poll_choices[$i]->content = $choice;

                        $poll_choices[$i]->save();

                    }
                    //Add new Choice because user added a choice
                    else {
                        poll_choices::create(['pollID' => $pollID, 'content' => $choice]);
                        audit_trail::create(['operation' => 'create', 'table' => 'poll_choices', 'voterID' => $userID, 'field' => 'pollID', 'new_value' => $poll->id]);
                        audit_trail::create(['operation' => 'create', 'table' => 'poll_choices', 'voterID' => $userID, 'field' => 'content', 'new_value' => $choice]);
                    }
                }
            }

            $poll_district_choices = poll_districts::where([['pollID', '=', $poll->id]])
                                    ->get();

            $districts = $poll_district_choices->pluck(["districtID"])->all();
            $districts = array_unique($districts);
            
            //Deal with which districts can see this Poll
            foreach($district_choices as $district_choice) {

                $elements = explode(":", $district_choice);
                
                //populate entire city
                if ($elements[0] == "City") {

                    $myDistricts = district::all();

                    foreach($myDistricts as $myDistrict) {
                        
                        //If not already in array
                        if (!in_array($myDistrict->id, $districts)) {

                            $poll_district = new Poll_Districts();
                            $poll_district->districtID = $myDistrict->id;
                            $poll_district->pollID = $pollID;
                            $poll_district->save();
                        }
                    }

                    //Break out of loop, we are done inserting since city encapsulates all districts at this time
                    return $this->showManagePollsView();
                }

                else if ($elements[0] == "Borough") {

                    //Get the Borough Name
                    $borough = urldecode($elements[1]);

                    $districtsCovered = DB::table('district_boroughs_maps')
                                    ->join('boroughs', 'boroughs.id', '=', 'district_boroughs_maps.boroughID')
                                    ->where('boroughs.boroughName', $borough)
                                    ->select('district_boroughs_maps.districtID')
                                    ->get();

                    foreach($districtsCovered as $myDistrict) {
                        
                        if (!in_array($myDistrict->id, $districts)) {

                            $poll_district = new Poll_Districts();
                            $poll_district->districtID = $myDistrict->districtID;
                            $poll_district->pollID = $pollID;
                            $poll_district->save();
                        }
                    }
                }

                else {

                    $district_type = $elements[0];
                    $district_num = $elements[1];
                    
                    switch($district_type) {
                        case 'Assembly':
                            $district = 'assemblyDistrict';
                            break;
                        case 'City Council':
                            $district = 'citycouncilDistrict';
                        break;
                        case 'Community':
                            $district = 'communityDistrict';
                            break;
                        case 'Congress':
                            $district = 'congressDistrict';
                            break;
                        case 'State Senator':
                            $district = 'stateSenatorialDistrict';
                            break;
                    }

                    //Get District ID
                    $district = district::where([['district_num', '=', $district_num], ['district_type', '=', $district]])->first();
                        
                    if (!in_array($district->id, $districts)) {

                        $poll_district = new Poll_Districts();
                        $poll_district->districtID = $district->id;
                        $poll_district->pollID = $pollID;
                        $poll_district->save();
                    }
                }
            }

            $poll->save();
        
        }

        //Create a new Poll instead
        else {

            //Save the Poll Object
            $poll = new poll;
            $poll->authorID = $userID;
            $poll->question = $poll_question;

            if (isset($sponsorTitle) && strlen($sponsorTitle) > 0) {
                $poll->sponsor = $sponsorTitle;
            }
            else {
                $poll->sponsor = "";
            }
            
            $poll->more = $more_info;
            $poll->link = $link;
            $poll->num_likes = 0;
            $poll->num_comments = 0;
            $poll->status = 1;

            //Assign a random hash, used when sharing the results of the Poll
            $random = Str::random(32);
            $poll->hashcode = $random;

            $poll->save();

            //Get the new Poll ID
            $pollID = $poll->id;
        
            //Save the Options
            foreach($choices_array as $choice) {
                //Validate that the Choice text for the poll isn't empty
                if (strlen($choice) > 0) {
                    poll_choices::create(['pollID' => $poll->id, 'content' => $choice]);
                    audit_trail::create(['operation' => 'create', 'table' => 'poll_choices', 'voterID' => $userID, 'field' => 'pollID', 'new_value' => $poll->id]);
                    audit_trail::create(['operation' => 'create', 'table' => 'poll_choices', 'voterID' => $userID, 'field' => 'content', 'new_value' => $choice]);
                }
            }

            //Deal with which districts can see this Poll
            foreach($district_choices as $district_choice) {

                $elements = explode(":", $district_choice);
                
                //populate entire city
                if ($elements[0] == "City") {

                    $myDistricts = district::all();

                    foreach($myDistricts as $myDistrict) {
                        
                        $poll_district = new Poll_Districts();
                        $poll_district->districtID = $myDistrict->id;
                        $poll_district->pollID = $pollID;
                        $poll_district->save();
                    }

                    //Break out of loop, we are done inserting since city encapsulates all districts at this time
                    return $this->showManagePollsView();
                }

                else if ($elements[0] == "Borough") {

                    //Get the Borough Name
                    $borough = urldecode($elements[1]);

                    $districtsCovered = DB::table('district_boroughs_maps')
                                    ->join('boroughs', 'boroughs.id', '=', 'district_boroughs_maps.boroughID')
                                    ->where('boroughs.boroughName', $borough)
                                    ->select('district_boroughs_maps.districtID')
                                    ->get();

                    foreach($districtsCovered as $myDistrict) {
                        
                        $poll_district = new Poll_Districts();
                        $poll_district->districtID = $myDistrict->districtID;
                        $poll_district->pollID = $pollID;
                        $poll_district->save();
                    }
                }

                else {

                    $district_type = $elements[0];
                    $district_num = $elements[1];

                    switch($district_type) {
                        case 'Assembly':
                            $district = 'assemblyDistrict';
                            break;
                        case 'City Council':
                            $district = 'citycouncilDistrict';
                            break;
                        case 'Community':
                            $district = 'communityDistrict';
                            break;
                        case 'Congress':
                            $district = 'congressDistrict';
                            break;
                        case 'State Senator':
                            $district = 'stateSenatorialDistrict';
                    }

                    //Get District ID
                    $district = district::where([['district_num', '=', $district_num]])->first();
                    
                    $poll_district = new Poll_Districts();
                    $poll_district->districtID = $district->id;
                    $poll_district->pollID = $pollID;
                    $poll_district->save();
                }
            }
        }

        return $this->showManagePollsView();

    }

    /**
     * This Function is meant for the Admininistrator/Moderator access for Polliticly's backend.
     * 
     * Calling this function gives the user an opportunity to update the poll
     * $request                          Blade Form file
     * 
     * Inside Request contains the Title question for the poll as well as an array of selected answers.
     */
    public function updatePoll($pollID) {
    
        $poll  = poll::find($pollID);

        $poll_choices = poll_choices::where([['pollID', '=', $pollID]])
                        ->get();


        //Get the Districts
        $assembly_districts = $this->getDistricts(23,87,[]);
        $citycouncil_districts = $this->getDistricts(1,51,[]);
        $congress_districts = $this->getDistricts(3,16,[]);
        $senate_districts = $this->getDistricts(10,36,[]);

        //Merge the Community Boards from the suburbs
        $c_manhattan = $this->getDistricts(101,112,[]);
        $c_bronx = $this->getDistricts(201,212,[]);
        $c_queens = $this->getDistricts(401,414,[]);
        $c_statenisland = $this->getDistricts(501,503,[]);
        $c_brooklyn = $this->getDistricts(301,318,[]);
        $community_districts = array_merge($c_manhattan, $c_bronx, $c_brooklyn, $c_queens, $c_statenisland);

        //Get Borough Names
        $results = DB::table('district_boroughs_maps')
                        ->join('boroughs', 'district_boroughs_maps.boroughID', '=', 'boroughs.id')
                        ->select('boroughs.boroughName', 'boroughs.city')
                        ->distinct('boroughs.id')
                        ->get();
        
        //Extract Cities
        $cities = $results->pluck(["city"])->all();
        $cities = array_unique($cities);

        $boroughs = $results->pluck(["boroughName"])->all();
        $boroughs = array_unique($boroughs);

        //Get current districts eligible for this poll
        $poll_districts = DB::table('poll_districts')
                            ->join('districts', 'poll_districts.districtID', '=', 'districts.id')
                            ->select('districts.district_num', 'districts.district_type')
                            ->where('poll_districts.pollID', $pollID)
                            ->get();

        foreach ($poll_districts as $myDistrict) {

            $district = "";

            switch($myDistrict->district_type) {
                case 'city':
                    $district = 'City';
                    break;
                case 'boroughs':
                    $district = 'Borough';
                    break;
                case 'assemblyDistrict':
                    $district = 'Assembly';
                    break;
                case 'cityCouncilDistrict':
                    $district = 'City Council';
                    break;
                case 'communityDistrict':
                    $district = 'Community';
                    break;
                case 'congressDistrict':
                    $district = 'Congress';
                    break;
                case 'stateSenatorialDistrict':
                    $district = 'State Senator';
                    break;
                default:
                    // code block
            }
            
            $myDistrict->district_type = $district;
        }

        return view('create_poll', ['poll' => $poll, 'choices' => $poll_choices, 'poll_district' => $poll_districts, 'assembly' => $assembly_districts, 'citycouncil' => $citycouncil_districts, 'community' => $community_districts, 'congress' => $congress_districts, 'senate' => $senate_districts, 'borough' => $boroughs, 'city' => $cities]);

    }

    /**
     * This Function is meant for the Admininistrator/Moderator access for Polliticly's backend.
     * 
     * Calling this function gives the user an opportunity to delete a poll
     * 
     * Inside Request contains the Title question for the poll as well as an array of selected answers.
     */
    public function deletePoll($pollID) {

        //The ID of the user on the PHP Admin System
        $userID = Auth::user()->id;

        //Delete Poll
        $poll  = poll::find($pollID);

        //Audit Trail
        audit_trail::create(['operation' => 'delete', 'table' => 'polls', 'voterID' => $userID, 'field' => 'id', 'old_value' => $pollID]);
        audit_trail::create(['operation' => 'delete', 'table' => 'polls', 'voterID' => $userID, 'field' => 'question', 'old_value' => $poll->question]);
        audit_trail::create(['operation' => 'delete', 'table' => 'polls', 'voterID' => $userID, 'field' => 'authorID', 'old_value' => $poll->authorID]);
        audit_trail::create(['operation' => 'delete', 'table' => 'polls', 'voterID' => $userID, 'field' => 'num_likes', 'old_value' => $poll->num_likes]);
        audit_trail::create(['operation' => 'delete', 'table' => 'polls', 'voterID' => $userID, 'field' => 'num_comments', 'old_value' => $poll->num_comments]);
        
        //Delete the Row
        $poll->delete();

        //Delete Poll Likes
        $poll_likes = poll_likes::where([['pollID', '=', $pollID]])
                      ->get();

        //Delete the Rows
        foreach ($poll_likes as $like) {
            //Audit Trail
            audit_trail::create(['operation' => 'delete', 'table' => 'poll_likes', 'voterID' => $userID, 'field' => 'pollID', 'old_value' => $like->pollID]);
            audit_trail::create(['operation' => 'delete', 'table' => 'poll_likes', 'voterID' => $userID, 'field' => 'voterID', 'old_value' => $like->voterID]);
             
            $like->delete();
        }

        //Delete Poll Comments
        $poll_comments = poll_comments::where([['pollID', '=', $pollID]])
                         ->get();

        //Delete the Rows
        foreach ($poll_comments as $comment) {
            //Audit Trail
            audit_trail::create(['operation' => 'delete', 'table' => 'poll_comments', 'voterID' => $userID, 'field' => 'pollID', 'old_value' => $comment->pollID]);
            audit_trail::create(['operation' => 'delete', 'table' => 'poll_comments', 'voterID' => $userID, 'field' => 'voterID', 'old_value' => $comment->voterID]);
            audit_trail::create(['operation' => 'delete', 'table' => 'poll_comments', 'voterID' => $userID, 'field' => 'comment', 'old_value' => $comment->comment]);

            $comment->delete();
        }

        //Delete Choices
        $poll_choices = poll_choices::where([['pollID', '=', $pollID]])
                        ->get();

        //Delete the Rows
        foreach ($poll_choices as $choice) {
            //Audit Trail
            audit_trail::create(['operation' => 'delete', 'table' => 'poll_choices', 'voterID' => $userID, 'field' => 'pollID', 'old_value' => $choice->pollID]);
            audit_trail::create(['operation' => 'delete', 'table' => 'poll_choices', 'voterID' => $userID, 'field' => 'content', 'old_value' => $choice->content]);

            $choice->delete();
        }

        //Delete User Selections
        $user_selections = vote_selections::where([['pollID', '=', $pollID]])
                        ->get();
        foreach ($user_selections as $selection) {
            //Audit Trail
            audit_trail::create(['operation' => 'delete', 'table' => 'vote_selections', 'voterID' => $userID, 'field' => 'pollID', 'old_value' => $selection->pollID]);
            audit_trail::create(['operation' => 'delete', 'table' => 'vote_selections', 'voterID' => $userID, 'field' => 'voterID', 'old_value' => $selection->voterID]);
            audit_trail::create(['operation' => 'delete', 'table' => 'vote_selections', 'voterID' => $userID, 'field' => 'selection', 'old_value' => $selection->selection]);

            $selection->delete();
        }

        //Return the View showing all the Polls
        return $this->showManagePollsView();

    }

     /**
     * This Function is meant for the Admininistrator/Moderator access for Polliticly's backend.
     * 
     * Calling this function show all comments made by other voters on this poll
     * 
     */
    public function comments ($pollID) {

        $active_comments = DB::table('poll_comments')
                            ->leftjoin('voters', 'poll_comments.voterID', '=', 'voters.id')
                            ->where('poll_comments.pollID', $pollID)
                            ->where('poll_comments.visible', 1)
                            ->select('poll_comments.id', 'voters.username', 'poll_comments.comment', 'poll_comments.created_at')
                            ->get();

        $deleted_comments = DB::table('poll_comments')
                            ->leftjoin('voters', 'poll_comments.voterID', '=', 'voters.id')
                            ->where('poll_comments.pollID', $pollID)
                            ->where('poll_comments.visible', 0)
                            ->select('poll_comments.id', 'voters.username', 'poll_comments.comment', 'poll_comments.created_at')
                            ->get();

        return view('comments', ['pollID' => $pollID, 'comments' => $active_comments, 'deleted_comments' => $deleted_comments]);
 
    }

    /**
     * This Function is meant for the Admininistrator/Moderator access for Polliticly's backend.
     * 
     * Calling this function show all comments made by other voters on this poll
     * 
     */
    public function hideComment ($commentid, $pollID) {

        $comment = poll_comments::find($commentid);
        $comment->visible = 0;
        $comment->save();

        //Audit Trail
        audit_trail::create(['operation' => 'update', 'table' => 'poll_comments', 'field' => 'visible', 'new_value' => 0]);
       

        return $this->comments($pollID);
 
    }

    
    /**
     * This function returns the View showing how admin can generate results pages
     * 
     */
    public function generateResults ($pollID) {

        $poll = poll::find($pollID);

        //Get District Numbers

        //Get the Districts
        $assembly_districts = $this->getDistricts(23,87,[]);
        $citycouncil_districts = $this->getDistricts(1,51,[]);
        $congress_districts = $this->getDistricts(3,16,[]);
        $senate_districts = $this->getDistricts(10,36,[]);

        //Get Borough Names
        $results = DB::table('poll_districts')
                        ->join('districts', 'poll_districts.districtID', '=', 'districts.id')
                        ->join('polls', 'poll_districts.pollID', '=', 'polls.id')
                        ->join('district_boroughs_maps', 'district_boroughs_maps.districtID', '=', 'districts.id')
                        ->join('boroughs', 'district_boroughs_maps.boroughID', '=', 'boroughs.id')
                        ->where('polls.id', $poll->id)
                        ->select('boroughs.boroughName', 'boroughs.city')
                        ->distinct('boroughs.id')
                        ->get();

        //Extract Cities
        $cities = $results->pluck(["city"])->all();
        $cities = array_unique($cities);

        $boroughs = $results->pluck(["boroughName"])->all();
        $boroughs = array_unique($boroughs);

        //Merge the Community Boards from the suburbs
        $c_manhattan = $this->getDistricts(101,112,[]);
        $c_bronx = $this->getDistricts(201,212,[]);
        $c_queens = $this->getDistricts(401,414,[]);
        $c_statenisland = $this->getDistricts(501,503,[]);
        $c_brooklyn = $this->getDistricts(301,318,[]);
        $community_districts = array_merge($c_manhattan, $c_bronx, $c_brooklyn, $c_queens, $c_statenisland);

        return view('generate_results', ['poll' => $poll, 'assembly' => $assembly_districts, 'citycouncil' => $citycouncil_districts, 'community' => $community_districts, 'congress' => $congress_districts, 'senate' => $senate_districts, 'borough' => $boroughs, 'city' => $cities]);

    }

     /**
     * Helper Function for listing Districts
     * 
     * $min               Minimum Number to start counting from
     * $max               Maximum Number to end at
     * $omit_array        Array containing values to omit
     */
    private function getDistricts($min, $max, $omit_array) {

        $countarray = [];
        
        for ($i = $min; $i < ($max + 1); $i++) {

            //Omit number from the omit_array
            if (!in_array($i, $omit_array)) {
                array_push($countarray, $i);
            }
        }

        return $countarray;
    }

    /*------------------------------- FrontEnd Calls ---------------------------------*/

    /**
     * This Function is an API call from the Frontend
     * 
     * Calling this function creates a Poll with it respective poll choices for Voters to choose from.
     * $request                          JSON 
     * 
     */
    public function createAPoll(Request $request) {
    
        //Read in the JSON object to get the Voter ID
        $json = $request->json()->all();
        
        $authorID       = $json['userID'];
        $title          = $json['question'];
        $choice1        = $json['choice1'];
        $choice2        = $json['choice2'];
        $choice3        = $json['choice3'];
        $choice4        = $json['choice4'];

        //Create a New Poll
        $new_poll = new poll();
        $new_poll->authorID = $authorID;
        $new_poll->question = $title;
        $new_poll->num_likes = 0;
        $new_poll->num_comments = 0;
        $new_poll->status = 0;

        //Assign a random hash, used when sharing the results of the Poll
        $random = Str::random(32);
        $new_poll->hashcode = $random;

        $new_poll->save();

        //Audit Trail
        audit_trail::create(['operation' => 'create', 'table' => 'polls', 'voterID' => $authorID, 'field' => 'question', 'new_value' => $title]);
        
        //Create the Selections
        $poll_choice = new poll_choices();
        $poll_choice->pollID = $new_poll->id;
        $poll_choice->content = $choice1;
        $poll_choice->save();

        //Create the Selections
        $poll_choice = new poll_choices();
        $poll_choice->pollID = $new_poll->id;
        $poll_choice->content = $choice2;
        $poll_choice->save();

        //Create the Selections
        $poll_choice = new poll_choices();
        $poll_choice->pollID = $new_poll->id;
        $poll_choice->content = $choice3;
        $poll_choice->save();

        //Create the Selections
        $poll_choice = new poll_choices();
        $poll_choice->pollID = $new_poll->id;
        $poll_choice->content = $choice4;
        $poll_choice->save();

        //Audit Trail
        audit_trail::create(['operation' => 'create', 'table' => 'poll_choices', 'voterID' => $authorID, 'field' => 'pollID', 'new_value' => $new_poll->id]);
        audit_trail::create(['operation' => 'create', 'table' => 'poll_choices', 'voterID' => $authorID, 'field' => 'content', 'new_value' => $choice1]);
        audit_trail::create(['operation' => 'create', 'table' => 'poll_choices', 'voterID' => $authorID, 'field' => 'content', 'new_value' => $choice2]);
        audit_trail::create(['operation' => 'create', 'table' => 'poll_choices', 'voterID' => $authorID, 'field' => 'content', 'new_value' => $choice3]);
        audit_trail::create(['operation' => 'create', 'table' => 'poll_choices', 'voterID' => $authorID, 'field' => 'content', 'new_value' => $choice4]);
    
        $data['code'] = 1;
        $data['msg']  = 'Poll Created';
        $data['data'] = '';

        return response()->json($data);   
    }

    /**
     * This Function returns the Poll information to the FrontEnd with selected choices
     * 
     * Return Values
     * 
     * polls -> The Poll Details (Question, Number of Likes, Number of Comments and Available Choices)
     * 
     * Inside each poll object contains
     * poll_likes -> Those who liked
     * poll_comments -> Those who commented and their comments
     * user_selection -> The selection the user made in this poll
     */
    public function returnPolls(Request $request) {

        //Read in the JSON object to get the Voter ID
        $json = $request->json()->all();

        $voterID        = $json['userID'];
        //$voterID = 1;

        //Grab the Poll IDs that the user is allowed to see
        $poll_ids = DB::table('poll_districts')
                    ->leftjoin('voter_districts', 'voter_districts.districtID', '=', 'poll_districts.districtID')
                    ->where('voter_districts.voterID', $voterID)
                    ->select('poll_districts.pollID')
                    ->groupBy('poll_districts.pollID')
                    ->get();

        //Change into 1-D array
        $ids = $poll_ids->pluck(["pollID"])->all();
        
        //Get the Poll information
        $polls = poll::where([['status', '=', 1]])
                        ->whereIn('id', $ids)
                        ->orderby('created_at', 'DESC')
                        ->get();

        //Save the Options
        foreach($polls as $poll) {
            
            //Pull Poll Likes from Database with matching voterID and PollID
            $poll_choices = poll_choices::where([['pollID', '=', $poll->id]])->get();
            //error_log($poll_choices);

            //Get the number of selections made for each choice
            foreach($poll_choices as $poll_choice) {

                $num_votes = vote_selections::where([['pollID', '=', $poll->id], ['selection', '=', $poll_choice->id]])->get();
                $poll_choice['num_votes'] = sizeof($num_votes);  //Get the number of votes recieved for this selection
            
                //error_log("Num Votes: " . $poll_choice);
            }
            

            //Get all the Likes from this Poll
            $poll_likes = poll_likes::where([['pollID', '=', $poll->id]])->get();
            //error_log($poll_likes);

            //Did this user like this poll?
            $user_liked = poll_likes::where([['voterID', '=', $voterID], ['pollID', '=', $poll->id]])->get();

            if (sizeof($user_liked) > 0) {
                $user_liked_poll = true;  //User liked this poll
            }

            //Get all the Comments from this Poll
            $poll_comments = poll_comments::where([['visible', '=', 1], ['pollID', '=', $poll->id]])->get();

            //Did this user leave a comment on this poll?
            $user_commented = poll_comments::where([['visible', '=', 1], ['voterID', '=', $voterID], ['pollID', '=', $poll->id]])->get();

            //error_log($poll_comments);

            //Get the user Selection object
            $user_selection = vote_selections::where([['voterID', '=', $voterID], ['pollID', '=', $poll->id]])->first();
            
            if (isset($user_selection)) {
                $user_selection = $user_selection->selection;
            }
            //error_log($user_selection);

            //The available choices the voter can choose from            
            $poll->poll_choices = $poll_choices;

            //Any likes on this poll?
            if (isset($poll_likes)) {
                $poll->poll_likes = $poll_likes;
            } else {
                $poll->poll_likes = [];
            }

            //Any comments on this poll?
            if (isset($poll_comments)) {
                $poll->poll_comments = $poll_comments;
            } else {
                $poll->poll_comments = [];

            }

            //Did the user make a selection for this poll?
            if (isset($user_selection)) {
                $poll->user_selection = $user_selection;
            } else {
                $poll->user_selection = -1;
            }

            //Did the user like this poll?
            if (isset($user_liked_poll)) {
                if (sizeof($user_liked) > 0) {
                    $poll->user_liked = true;
                } else {
                    $poll->user_liked = false;
                }
            }

            //Did the user comment on this poll?
            if (isset($user_commented)) {
                $poll->user_comments = $user_commented;
            } else {
                $poll->user_comments = [];
            }
            
        }

        //error_log("Polls Sent: " . $polls);

        $data['code'] = 1;
        $data['msg']  = 'Polls Retrieved';
        $data['data'] = $polls;

        //dd($data);

        return response()->json($data);   
    }

    /**********************************************************************************
     * When the user casts a Vote
     */
    public function castVote (Request $request) {

        //Read in the JSON object to get the email and password of the account
        $json = $request->json()->all();

        $voterID        = $json['userID'];
        $pollID         = $json['pollID'];
        $selection      = $json['selection'];

        error_log("VoterID: " . $voterID );
        error_log("pollID: " . $pollID );

        $vote_selected = vote_selections::where([['voterID', '=', $voterID], ['pollID', '=', $pollID]])->first();

        //Update Vote Selection
        if (isset($vote_selected)) {

            $vote_selected->selection = $selection;
            $vote_selected->save();

            //Audit Trail
            audit_trail::create(['operation' => 'update', 'table' => 'vote_selections', 'voterID' => $voterID, 'field' => 'selection', 'new_value' => $selection]);

            $data['code'] = 1;
            $data['msg']  = 'Vote Updated';
            $data['data'] = '';

            return response()->json($data); 
        }

        //Cast the Vote
        else {

            $vote_selection = new vote_selections;
            $vote_selection->voterID = $voterID;
            $vote_selection->pollID  = $pollID;
            $vote_selection->selection = $selection;
            $vote_selection->save();

            //Audit Trail
            audit_trail::create(['operation' => 'create', 'table' => 'vote_selections', 'voterID' => $voterID, 'field' => 'pollID', 'new_value' => $pollID]);
            audit_trail::create(['operation' => 'create', 'table' => 'vote_selections', 'voterID' => $voterID, 'field' => 'selection', 'new_value' => $selection]);

            $data['code'] = 1;
            $data['msg']  = 'Vote Casted';
            $data['data'] = '';

            return response()->json($data); 
        }

        
    }


    /****
     * Given a Poll ID, we return all the Comments from that poll
     */
    public function getComments(Request $request) {

        //Read in the JSON object to get the Voter ID
        $json = $request->json()->all();
    
        $pollID        = $json['pollID'];

        //Get Poll Comments from the Poll ID
        $poll_comments = DB::table('poll_comments')
                        ->leftjoin('voters', 'poll_comments.voterID', '=', 'voters.id')
                        ->where('poll_comments.pollID', $pollID)
                        ->where('visible', 1)
                        ->select('poll_comments.*', 'voters.username')
                        ->orderBy('poll_comments.created_at', 'ASC')
                        ->get();

        //error_log($poll_comments);

        if (isset($poll_comments)) {

            $data['code'] = 1;
            $data['msg']  = 'Comments Retrieved';
            $data['data'] = $poll_comments;

            return response()->json($data);   
        }
        else {

            $data['code'] = 0;
            $data['msg']  = 'No Comments Found';
            $data['data'] = '';

            return response()->json($data);   
        }
    }

    /****
     * This Function adds a like to the Poll
     */
    public function addLike (Request $request) {

        //Read in the JSON object to get the Voter ID
        $json = $request->json()->all();

        $voterID        = $json['userID'];
        $pollID         = $json['pollID'];

        $poll = poll::find($pollID);

        //Error Check
        $error_check = poll_likes::where([['voterID', '=', $voterID], ['pollID', '=', $pollID]])->first();

        //If we have a duplicate entry
        if (isset($error_check)) {

            $data['code'] = 0;
            $data['msg']  = 'Duplicate Like';
            $data['data'] = '';

            return response()->json($data);   
        }
        else {
            //Add to Like total
            $poll->num_likes = $poll->num_likes + 1;
            $poll->save();

            //Create the Like Object
            $poll_like_row = new poll_likes;
            $poll_like_row->pollID = $pollID;
            $poll_like_row->voterID = $voterID;
            $poll_like_row->save();

            //Audit Trail
            audit_trail::create(['operation' => 'create', 'table' => 'poll_likes', 'voterID' => $voterID, 'field' => 'pollID', 'new_value' => $pollID]);
    
            $data['code'] = 1;
            $data['msg']  = 'Like Added';
            $data['data'] = '';

            return response()->json($data);   
        }
    }

    /****
     * This Function adds a like to the Poll
     */
    public function removeLike (Request $request) {

        //Read in the JSON object to get the Voter ID
        $json = $request->json()->all();

        $voterID        = $json['userID'];
        $pollID         = $json['pollID'];

        $poll = poll::find($pollID);

        //Error Check
        $entry = poll_likes::where([['voterID', '=', $voterID], ['pollID', '=', $pollID]])->first();

        //If we have found an entry
        if (isset($entry)) {

            //Remove a Like from the total
            $poll->num_likes = $poll->num_likes - 1;
            $poll->save();

            //Audit Trail
            audit_trail::create(['operation' => 'delete', 'table' => 'poll_likes', 'voterID' => $voterID, 'field' => 'pollID', 'old_value' => $pollID]);

            //Delete the Entry
            $entry->delete();

            $data['code'] = 1;
            $data['msg']  = 'Removed Like';
            $data['data'] = '';

            return response()->json($data);   
        }
        else {
    
            $data['code'] = 0;
            $data['msg']  = 'Entry Not Found';
            $data['data'] = '';

            return response()->json($data);   
        }
    }

    /****
     * This Function adds a Comment to a Poll
     */
    public function addComment (Request $request) {

        //Read in the JSON object to get the Voter ID
        $json = $request->json()->all();

        $voterID        = $json['userID'];
        $pollID         = $json['pollID'];
        $comment        = $json['comment'];

        $poll = poll::find($pollID);

        //Add to Commment Total
        $poll->num_comments = $poll->num_comments + 1;
        $poll->save();

        //Create the Comment Row
        $poll_comment_row = new poll_comments;
        $poll_comment_row->polliD = $pollID;
        $poll_comment_row->voterID = $voterID;
        $poll_comment_row->comment = $comment;
        $poll_comment_row->visible = 1;
        $poll_comment_row->save();

        //Get the User Name and send it back with the object
        $voter = voters::find($voterID);
        $poll_comment_row->username = $voter->username;

        //Audit Trail
        audit_trail::create(['operation' => 'create', 'table' => 'poll_comments', 'voterID' => $voterID, 'field' => 'pollID', 'new_value' => $pollID]);
        audit_trail::create(['operation' => 'create', 'table' => 'poll_comments', 'voterID' => $voterID, 'field' => 'comment', 'new_value' => $comment]);

        $data['code'] = 1;
        $data['msg']  = 'Comment Added';
        $data['data'] = $poll_comment_row;

        return response()->json($data);   
    }

    /****
     * This Function hides a Comment on the Poll
     */
    public function removeComment (Request $request) {

        //Read in the JSON object to get the Voter ID
        $json = $request->json()->all();

        $voterID        = $json['userID'];
        $pollID         = $json['pollID'];
        $commentID      = $json['commentID'];

        $poll = poll::find($pollID);

        //Remove a Commment from the Total
        $poll->num_comments = $poll->num_comments - 1;
        $poll->save();

        //Create the Comment Row
        $poll_comment = poll_comments::find($commentID);
        $poll_comment->visible = 0;
        $poll_comment->save();

        //Audit Trail
        audit_trail::create(['operation' => 'update', 'table' => 'poll_comments', 'voterID' => $voterID, 'field' => 'visible', 'old_value' => '1', 'new_value' => '0']);

        $data['code'] = 1;
        $data['msg']  = 'Comment Removed';
        $data['data'] = '';

        return response()->json($data);   
    }


    public function setPollDistrictMap() {

        for ($y = 19; $y < 28; $y++) {
            for ($x = 1; $x < 213; $x++) {
                
                $poll_districts = new Poll_Districts();
                $poll_districts->pollID = $y;
                $poll_districts->districtID = $x;
                $poll_districts->save();
            }
        }
    }
}
