<?php

namespace App\Http\Controllers;

use App\Models\audit_trail;
use App\Models\voters;
use App\Models\district;
use App\Models\firebase_push_notifications;
use App\Models\voter_district;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use UpdateVotersTable;

class VoterController extends Controller
{
    //New York API URL
    const NY_GEOCLIENT_ADDRESS_URL = 'https://api.cityofnewyork.us/geoclient/v1/address.json?houseNumber=%s&street=%s&zip=%s&app_id=%s&app_key=%s';
    
    //Constants
    const Assembly = 'assemblyDistrict';
    const Senator  = 'stateSenatorialDistrict';
    const CityCouncil = 'cityCouncilDistrict';
    const Congress    = 'congressionalDistrict';
    const Community   = 'communityDistrict';

    /****************************************
     * Show all Voters.
     *
     */
    public function index()
    {

        $voters = voters::all();
        //return view('home');
    }

    /*****************************************
     * This API call takes in a JSON object 
     * and creates or updates a Voter. 
     * 
     * If an ID is provided in the JSON, we assume
     * it's an update, otherwise we create a new
     * entry.
     * 
     */
    public function editVoterInfo(Request $request) {

        //Read in the JSON Object and decode into an array
        $json = $request->json()->all();

        //JSON objects
        if (isset($json['userID'])) {
            $voterID             = $json['userID'];
            error_log("UserID: " . $voterID);
        } 

        //Updating Fields
        if (isset($voterID)) {
            $voter = voters::find($voterID);
        }
        else {
            $voter = new voters;
            $password   = Hash::make($json['password']);  //Encrypt Password
        }

        //Audit Trail Entry  
        if (isset($voterID)) {
            //If First Name is updated
            if ($voter->first_name != $json['first_name']) {
                audit_trail::create(['operation' => 'update', 'table' => 'voters', 'field' => 'first_name', 'voterID' => $voterID, 'old_value' => $voter->first_name, 'new_value' => $json['first_name']]);
            }
            //Last Name is updated
            if ($voter->last_name != $json['last_name']) {
                audit_trail::create(['operation' => 'update', 'table' => 'voters', 'field' => 'last_name', 'voterID' => $voterID, 'old_value' => $voter->last_name, 'new_value' => $json['last_name']]);
            }
            //Username is updated
            if ($voter->username != $json['username']) {
                audit_trail::create(['operation' => 'update', 'table' => 'voters', 'field' => 'username', 'voterID' => $voterID, 'old_value' => $voter->username, 'new_value' => $json['username']]);
            }
            //Email is updated
            if ($voter->email != $json['email']) {
                audit_trail::create(['operation' => 'update', 'table' => 'voters', 'field' => 'email', 'voterID' => $voterID, 'old_value' => $voter->email, 'new_value' => $json['email']]);
            }
            //Birthday is updated
            if ($voter->birthday != $json['birthday']) {
                audit_trail::create(['operation' => 'update', 'table' => 'voters', 'field' => 'birthday', 'voterID' => $voterID, 'old_value' => $voter->birthday, 'new_value' => $json['birthday']]);
            }
            //Ethnicity is updated
            if ($voter->ethnicity != $json['ethnicity']) {
                audit_trail::create(['operation' => 'update', 'table' => 'voters', 'field' => 'ethnicity', 'voterID' => $voterID, 'old_value' => $voter->ethnicity, 'new_value' => $json['ethnicity']]);
            }
            //Gender is updated
            if ($voter->gender != $json['gender']) {
                audit_trail::create(['operation' => 'update', 'table' => 'voters', 'field' => 'gender', 'voterID' => $voterID, 'old_value' => $voter->gender, 'new_value' => $json['gender']]);
            }
            //Avatar is updated
            if ($voter->avatar != $json['avatar']) {
                audit_trail::create(['operation' => 'update', 'table' => 'voters', 'field' => 'avatar', 'voterID' => $voterID, 'old_value' => $voter->avatar, 'new_value' => $json['avatar']]);
            }
            //House Num is updated
            if ($voter->house_num != $json['house_num']) {
                audit_trail::create(['operation' => 'update', 'table' => 'voters', 'field' => 'house_num', 'voterID' => $voterID, 'old_value' => $voter->house_num, 'new_value' => $json['house_num']]);
            
                //Flag address change
                $changed_address = true;
            }
            //Street is updated
            if ($voter->street != $json['street']) {
                audit_trail::create(['operation' => 'update', 'table' => 'voters', 'field' => 'street', 'voterID' => $voterID, 'old_value' => $voter->street, 'new_value' => $json['street']]);
               
                //Flag address change
                $changed_address = true;
            }
            //City is updated
            if ($voter->city != $json['city']) {
                audit_trail::create(['operation' => 'update', 'table' => 'voters', 'field' => 'city', 'voterID' => $voterID, 'old_value' => $voter->city, 'new_value' => $json['city']]);
            }
            //State is updated
            if ($voter->state != $json['state']) {
                audit_trail::create(['operation' => 'update', 'table' => 'voters', 'field' => 'state', 'voterID' => $voterID, 'old_value' => $voter->state, 'new_value' => $json['state']]);
            }
            //ZipCode is updated
            if ($voter->zipCode != $json['zipCode']) {
                audit_trail::create(['operation' => 'update', 'table' => 'voters', 'field' => 'zipCode', 'voterID' => $voterID, 'old_value' => $voter->zipCode, 'new_value' => $json['zipCode']]);
            }
        }
        //New Entry
        else {
            //Flag address change
            $changed_address = true;

            audit_trail::create(['operation' => 'create', 'table' => 'voters', 'field' => 'first_name', 'new_value' => $json['first_name']]);
            audit_trail::create(['operation' => 'create', 'table' => 'voters', 'field' => 'last_name', 'new_value' => $json['last_name']]);
            audit_trail::create(['operation' => 'create', 'table' => 'voters', 'field' => 'username', 'new_value' => $json['username']]);
            audit_trail::create(['operation' => 'create', 'table' => 'voters', 'field' => 'email', 'new_value' => $json['email']]);
            audit_trail::create(['operation' => 'create', 'table' => 'voters', 'field' => 'birthday', 'new_value' => $json['birthday']]);
            audit_trail::create(['operation' => 'create', 'table' => 'voters', 'field' => 'ethnicity', 'new_value' => $json['ethnicity']]);
            audit_trail::create(['operation' => 'create', 'table' => 'voters', 'field' => 'gender', 'new_value' => $json['gender']]);
            audit_trail::create(['operation' => 'create', 'table' => 'voters', 'field' => 'avatar', 'new_value' => $json['avatar']]);
            audit_trail::create(['operation' => 'create', 'table' => 'voters', 'field' => 'house_num', 'new_value' => $json['house_num']]);
            audit_trail::create(['operation' => 'create', 'table' => 'voters', 'field' => 'street', 'new_value' => $json['street']]);
            audit_trail::create(['operation' => 'create', 'table' => 'voters', 'field' => 'city', 'new_value' => $json['city']]);
            audit_trail::create(['operation' => 'create', 'table' => 'voters', 'field' => 'state', 'new_value' => $json['state']]);
            audit_trail::create(['operation' => 'create', 'table' => 'voters', 'field' => 'zipCode', 'new_value' => $json['zipCode']]);
        }

        //Assign Values to Database Object
        $voter->first_name = $json['first_name'];
        $voter->last_name  = $json['last_name'];
        $voter->username   = $json['username'];

        //When updating, we never send back the password
        if (isset($password)) {
            $voter->password   = $password;
        }

        $voter->email      = $json['email'];
        $voter->birthday   = $json['birthday'];
        $voter->ethnicity  = $json['ethnicity'];
        $voter->gender     = $json['gender'];
        $voter->avatar     = $json['avatar'];
        $voter->house_num  = $json['house_num'];
        $voter->street     = $json['street'];
        $voter->city       = $json['city'];
        $voter->state      = $json['state'];
        $voter->zipCode    = $json['zipCode'];

        error_log("Email: " . $voter->email);

        //Save to Database
        $voter->save();

        $voterID      = $voter->id;
        $voterHouseNo = $json['house_num'];
        $voterStreet  = $json['street'];
        $zipCode      = $json['zipCode'];

        //Set Districts
        if (isset($changed_address)) {
            if (($changed_address) && ($voter->state === 'New York')) {

                //Call this function to find the new Districts 
                $this->setVoterDistricts($voterID, $voterHouseNo, $voterStreet , $zipCode);

                //Add Push Notification Information as well
                $push_notification_user = new firebase_push_notifications;

                //Save Object
                $push_notification_user->voterID = $voterID;
                $push_notification_user->unsubscribe = 0;
                $push_notification_user->token = $json['deviceToken'];
                $push_notification_user->save();

                //Audit Trail
                audit_trail::create(['operation' => 'create', 'table' => 'firebase_push_notifications', 'voterID' => $voterID, 'new_value' => $json['deviceToken']]);
            }
        }
    
        $data['code'] = 1;
        $data['msg']  = 'Successfully Created or Updated';
        $data['data'] = $voter;

        return response()->json($data);   
    }

    /*****************************************
     * Retrieve the Voter Details
     */
    public function getVoterInfo(Request $request) {

        //Read in the JSON object to get the email of the account
        $params = $request->input('params');
        $paramsObj = json_decode($params);

        $email = $paramsObj->email;

        //Find the entry by email 
        $voter = voters::where([
            ['email', '=', $email]
        ])->first();

        //If we were able to find a record return the voter object
        if ($voter) {

            $data = array();
            $data['code'] = 1;
            $data['msg']  = 'Voter Found';
            $data['data'] = $voter;
    
            return response()->json($data);   
        }
    }

    /********************************************
     * Checks password
     */
    public function validate_login(Request $request) {

        //Read in the JSON object to get the email and password of the account
        $json = $request->json()->all();

        $email      = $json['email'];
        $password   = $json['password'];

        error_log("Email: " . $email);
        error_log("Password: " . $password);

        //Find the entry by email 
        $voter = voters::where([
            ['email', '=', $email]
        ])->first();
  
        $data = array();

        //If we cannot find the email
        if (! $voter) {

            error_log("Voter Not Found");

            $data['code'] = 0;
            $data['msg']  = 'Voter Not Found';
            $data['data'] = '';
        }    

        //Otherwise Voter was found and we just need to check if password was correct
        else {
            $validate = Hash::check($password, $voter->password);

            if ($validate == true) {

                $data['code'] = 1;
                $data['msg']  = 'Login Success';
                $data['data'] = $voter;
            }
            else {
                $data['code'] = 0;
                $data['msg']  = 'Password incorrect';
                $data['data'] = '';
            }
        }
    
        return response()->json($data);   
    }


    private function setVoterDistricts($voterID, $voterHouseNo, $voterStreet , $zipCode) {

        //Delete Old Districts
        voter_district::where('voterID', $voterID)->delete();

        //First we need to get our App ID and App Key from the .ENV file
        $app_id = env('NY_GEOCLIENT_APPID');
        $app_key = env ('NY_GEOCLIENT_APPKEY');

        $geoclient_url = sprintf(self::NY_GEOCLIENT_ADDRESS_URL, $voterHouseNo, $voterStreet, $zipCode, $app_id, $app_key);

        //error_log($geoclient_url);

        //Run this through the New York API
        $response = Http::get($geoclient_url);
        $geoclientObj = $response->json();

        error_log("Assembly District: " . $geoclientObj['address'][self::Assembly]);
        error_log("Senator District: " . $geoclientObj['address'][self::Senator]);
        error_log("City Council District: " . $geoclientObj['address'][self::CityCouncil]);
        error_log("Congress District: " . $geoclientObj['address'][self::Congress]);
        error_log("Community District: " . $geoclientObj['address'][self::Community]);

        //Create the Object to save to DB
        $voter_district = new voter_district;
        $voter_district->voterID = $voterID;

        /******  Lookup the district numbers *******/
        //Assembly Object
        $assemblydistrict = district::where('district_num', $geoclientObj['address'][self::Assembly])
                ->where('district_type', self::Assembly)
                ->first();
        error_log("Assembly ID: " . $assemblydistrict->id);
        //Add the New District
        voter_district::create(['voterID' => $voterID, 'districtID' => $assemblydistrict->id]);

        //$voter_district->districtID = $assemblydistrict->id;
        //$voter_district->save();

        //Senator Object
        $senatordistrict = district::where('district_num', $geoclientObj['address'][self::Senator])
                ->where('district_type', self::Senator)
                ->first();
        error_log("Senator ID: " . $senatordistrict->id);
        voter_district::create(['voterID' => $voterID, 'districtID' => $senatordistrict->id]);

        //$voter_district->districtID = $senatordistrict->id;
        //$voter_district->save();

        //City Council Object
        $citycouncil_district = district::where('district_num', $geoclientObj['address'][self::CityCouncil])
                ->where('district_type', self::CityCouncil)
                ->first();
        error_log("City Council ID: " . $citycouncil_district->id);
        voter_district::create(['voterID' => $voterID, 'districtID' => $citycouncil_district->id]);

        //$voter_district->districtID = $citycouncil_district->id;
        //$voter_district->save();

        //Congress Object
        $congress_district = district::where('district_num', $geoclientObj['address'][self::Congress])
                ->where('district_type', self::Congress)
                ->first();
        error_log("Congress ID: " . $congress_district->id);
        voter_district::create(['voterID' => $voterID, 'districtID' => $congress_district->id]);

        //$voter_district->districtID = $congress_district->id;
        //$voter_district->save();

        //Community Object
        $community_district = district::where('district_num', $geoclientObj['address'][self::Community])
                ->where('district_type', self::Community)
                ->first();
        error_log("Community ID: " . $community_district->id);
        voter_district::create(['voterID' => $voterID, 'districtID' => $community_district->id]);

        //$voter_district->districtID = $community_district->id;
        //$voter_district->save();

        //Audit Trails
        audit_trail::create(['operation' => 'delete', 'table' => 'voter_districts', 'voterID' => $voterID]);
        audit_trail::create(['operation' => 'create', 'table' => 'voter_districts', 'voterID' => $voterID, 'field' => 'districtID', 'new_value' => $assemblydistrict->id]);
        audit_trail::create(['operation' => 'create', 'table' => 'voter_districts', 'voterID' => $voterID, 'field' => 'districtID', 'new_value' => $senatordistrict->id]);
        audit_trail::create(['operation' => 'create', 'table' => 'voter_districts', 'voterID' => $voterID, 'field' => 'districtID', 'new_value' => $citycouncil_district->id]);
        audit_trail::create(['operation' => 'create', 'table' => 'voter_districts', 'voterID' => $voterID, 'field' => 'districtID', 'new_value' => $congress_district->id]);
        audit_trail::create(['operation' => 'create', 'table' => 'voter_districts', 'voterID' => $voterID, 'field' => 'districtID', 'new_value' => $community_district->id]);

    }

    /********************************************
     * Changes Voter password
     */
    public function change_voter_password(Request $request) {

        //Read in the JSON object to get the email and password of the account
        $json = $request->json()->all();

        $voterID        = $json['userID'];
        $old_password   = $json['old_password'];
        $new_password   = $json['new_password'];

        error_log("ID: " . $voterID);
        error_log("Old PW: " . $old_password);
        error_log("New PW: " . $new_password);

        if (isset($voterID)) {

            //Find the Voter based on the ID
            $voter = voters::find($voterID);

            error_log("In Statement, ID: " . $voter->id );
            error_log("In Statement, Email: " . $voter->email );

            //Validate the Password first
            $validate = Hash::check($old_password, $voter->password);

            if ($validate == true) {

                error_log("In Statement, ID: " . $voter->id);

                $voter->password = Hash::make($new_password);  //Encrypt New Password
                //$voter->password = $new_password;
                $voter->save();

                //Audit Trail
                audit_trail::create(['operation' => 'update', 'table' => 'voters', 'voterID' => $voterID, 'field' => 'password']);

                $data['code'] = 1;
                $data['msg']  = 'Password Successfully Changed';
                $data['data'] = '';
            }
            else {
                $data['code'] = 0;
                $data['msg']  = 'Password incorrect';
                $data['data'] = '';
            }

            return response()->json($data);   
        }
    }
    
    /********************************************
     * Changes Voter password
     */
    public function set_notification_token(Request $request) {
    
        //Read in the JSON object to get the email and password of the account
        $json = $request->json()->all();

        $voterID        = $json['userID'];
        $token          = $json['token'];

        if (isset($voterID)) {

            //Check to see if we already have an entry
            //Find the entry by email 
            $user_token = firebase_push_notifications::where([['voterID', '=', $voterID]])->first();
            
            if ($user_token) {

                //Audit Trail
                audit_trail::create(['operation' => 'update', 'table' => 'firebase_push_notifications', 'field' => 'token', 'voterID' => $voterID, 'old_value' => $user_token->token, 'new_value' => $token]);

                $user_token->token = $token;
                $user_token->save();

                $data['code'] = 1;
                $data['msg']  = 'Token Successful updated';
                $data['data'] = '';
            }

            else {

                //Audit Trail
                audit_trail::create(['operation' => 'create', 'table' => 'firebase_push_notifications', 'voterID' => $voterID, 'new_value' => $token]);

                $new_notification = new firebase_push_notifications;
                $new_notification->voterID = $voterID;
                $new_notification->token   = $token;
                $new_notification->save();

                $data['code'] = 1;
                $data['msg']  = 'Token Successful created';
                $data['data'] = '';
            }
        }
        else {

            $data['code'] = 0;
            $data['msg']  = 'Not User Infomation Recieved';
            $data['data'] = '';
        }

        return response()->json($data);   

    }

    /**************************************************************
     * This function assembles all the Voters that have incorrectly inputted their address
     * Hence they do not have District information assigned
     */
    public function assembleAddressErrors(Request $request) {

        $voters_with_errors = DB::table('voters')
                                ->leftjoin('voter_districts', 'voters.id', '=', 'voter_districts.voterID')
                                ->select('voters.*')
                                ->where('voters.state', '=', 'New York')
                                ->WhereNull('voter_districts.id')
                                ->get();      

        return view('fix_districts', ['voters' => $voters_with_errors]);

    }

    /**************************************************************
     * This Function updates the Voter address and then runs the District info 
     * on the user. Called from the Admin Panel
     */
    public function updateVoterDistricts(Request $request) {

        $voterID         = $request->input('voter_id');
        $house_num       = $request->input('house_num');
        $street          = $request->input('st_name');
        $zipCode         = $request->input('zipCode');

        //Pull up the Voter
        $voter = voters::find($voterID);
        $voter->house_num = $house_num;
        $voter->street = $street;
        $voter->zipCode = $zipCode;
        $voter->save();

        //Call this function to find the new Districts 
        $this->setVoterDistricts($voterID, $house_num, $street, $zipCode);

        //Get district info
        $district_info = voter_district::where([['voterID', '=', $voterID]]);
        
        //We have Values
        if ($district_info->count()) {

           return "District Info Successfully Created";
        }
        else {
            return "Could not add District Information";
        }
    }
}
