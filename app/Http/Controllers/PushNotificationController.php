<?php

namespace App\Http\Controllers;

use App\Mail\NotificationEmail;
use App\Mail\UnsubscribedEmail;
use App\Models\audit_trail;
use App\Models\district;
use App\Models\firebase_push_notifications;
use App\Models\voter_district;
use App\Models\voters;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


class PushNotificationController extends Controller
{

    /**
     * This Function is called from the Admin Panel
     *
     * With this Function, we return the View that allows the admin to send a Push Notification
     */
    public function index() { 

        //Get the Districts
        $assembly_districts = $this->getDistricts(23,87,[]);
        $citycouncil_districts = $this->getDistricts(1,51,[]);
        $congress_districts = $this->getDistricts(3,16,[]);
        $senate_districts = $this->getDistricts(10,36,[]);

        //Merge the Community Boards from the suburbs
        $c_manhattan = $this->getDistricts(101,112,[]);
        $c_bronx = $this->getDistricts(201,212,[]);
        $c_queens = $this->getDistricts(401,414,[]);
        $c_statenisland = $this->getDistricts(501,503,[]);
        $c_brooklyn = $this->getDistricts(301,318,[]);
        $community_districts = array_merge($c_manhattan, $c_bronx, $c_brooklyn, $c_queens, $c_statenisland);

        //Get the number of user within each District Number based on the District Type
        $district_stats = [];

        //Stores the Districts by Grouping
        $temp_array = voter_district::groupBy('districtID')
                        ->selectRaw('count(*) as total, districtID')
                        ->get();
        
        //Gets all the Districts
        $districts = district::all();

        //ReOrganize Districts into a Reference Array -> Array[$DistrictID] = $DistrictObject
        $new_district_array = [];

        foreach ($districts as $district) {
            $new_district_array[$district->id] = $district;
        }

        //Populate our new Array of District Type and District Number as a count
        //Ex. $district_stats["assemblyDistrict_23"] = 1
        foreach ($temp_array as $element) {

            $district_info = $new_district_array[$element->districtID]->district_type . "_" . strval($new_district_array[$element->districtID]->district_num);
            $district_stats[$district_info] = $element->total;

        }
        
        //Variable to hold Total number of voters
        $total_voters = firebase_push_notifications::all();
        $total_voters = count($total_voters);

        return view('create_push_notification', ['assembly' => $assembly_districts, 'citycouncil' => $citycouncil_districts, 'community' => $community_districts, 'congress' => $congress_districts, 'senate' => $senate_districts, 'district_stats' => $district_stats, 'total' => $total_voters]);
    }

    /**
     * This Function is called from the Admin Panel
     * 
     * With this Function, we send out Push Notifications based on District Information
     */
    public function sendPN(Request $request) { 
    
        $messageTitle        = $request->input('message_title');
        $message             = $request->input('message');
        $districtNum         = $request->input('district_num');
        $districtType         = $request->input('district_type');
        
        //Array of all Members
        $all_members = [];

        if ($districtType == 'all') {

            //Get the Users we need to send a Push Notification to
            $all_members = DB::table('voters')
                        ->leftjoin('firebase_push_notifications', 'voters.id', '=', 'firebase_push_notifications.voterID')
                        ->select('voters.first_name', 'voters.last_name', 'voters.email', 'firebase_push_notifications.token', 'firebase_push_notifications.unsubscribe')
                        ->where('firebase_push_notifications.token', '<>', 'NULL')
                        ->distinct('voters.id')
                        ->get();            
        }

        else {
            //Get the District IDs
            $districts = district::where([['district_num' , '=', $districtNum], ['district_type', '=', $districtType]])->get();

            foreach($districts as $district) {

                $id = $district->id;

                //Get the Users we need to send a Push Notification to
                $members = DB::table('voter_districts')
                            ->leftjoin('voters', 'voter_districts.voterID', '=', 'voters.id')
                            ->leftjoin('firebase_push_notifications', 'voter_districts.voterID', '=', 'firebase_push_notifications.voterID')
                            ->where('voter_districts.districtID', $id)                        
                            ->select('voters.first_name', 'voters.last_name', 'voters.email', 'firebase_push_notifications.token', 'firebase_push_notifications.unsubscribe')
                            ->distinct('voters.id')
                            ->get();

                array_push($all_members, $members);
            }
        }

        error_log($all_members);

        //Array of Responses
        $responses = [];

        foreach ($all_members as $member) {

            //Send the Notification and store the responses
            array_push($responses, $this->sendNotification($messageTitle, $message, $member->token));
        
            if (! $member->unsubscribe) {
                //Send Mail to User as well
                Mail::to($member->email)->send(new NotificationEmail($member->first_name . " " . $member->last_name, $messageTitle, $message, $member->email));
            }
        }

        //No Return statement because Javascript controls the return View already - see create_push_notification.blade.php
    }

    /**
     * Helper Function for listing Districts
     * 
     * $min               Minimum Number to start counting from
     * $max               Maximum Number to end at
     * $omit_array        Array containing values to omit
     */
    private function getDistricts($min, $max, $omit_array) {

        $countarray = [];
        
        for ($i = $min; $i < ($max + 1); $i++) {

            //Omit number from the omit_array
            if (!in_array($i, $omit_array)) {
                array_push($countarray, $i);
            }
        }

        return $countarray;
    }

    /**
     * Send Firebase Notification to User
     *
     * 
     **/
    public function sendNotification($title, $message, $token) {          

        $SERVER_API_KEY = 'AAAANOK81lU:APA91bHw5yLA_KyjeZTHR510usoOES1P_LdVjUdXfPJdoQgQqkW48xJXZALHpN91x7yFP712E-kJkMecT0BhsCVMBndiFzTNfyW3tdLtarOzEluSyNW1GFna4pFs4OzLa0BcOHiymQwH';

        $data = [
            "registration_ids" => array($token),

            "notification" => [
                "title" => $title,
                "body" => $message,  
            ]
        ];

        $dataString = json_encode($data);

        $headers = [

            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        $response = curl_exec($ch);

        return $response;
    }

    /**
     * Function to unsubscribe users from getting emails
     * 
     * $email             Email of the USer
     */
    public function unsubscribe($email) {

        $voter = voters::where([['email', '=', $email]])->first();

        $firebase_user = firebase_push_notifications::where([['voterID', '=', $voter->id]])->first();

        if (isset($voter)) {

            //Set the Flag to unsubscribe to be true
            $firebase_user->unsubscribe = 1;
            $firebase_user->save();

            //Audit Trail
            audit_trail::create(['operation' => 'update', 'table' => 'firebase_push_notification', 'voterID' => $voter->id, 'field' => 'unsubscribe', 'new_value' => 1]);
        }

        //Send the user the Successfully Unsubscribed View
        Mail::to($voter->email)->send(new UnsubscribedEmail($voter->email));

        return $voter->email . " has been sucessfully unsubscribed";

    }

}
