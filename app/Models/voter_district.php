<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class voter_district extends Model
{
    use HasFactory;

    protected $table = 'voter_districts';

    protected $fillable = ['voterID', 'districtID'];

}
