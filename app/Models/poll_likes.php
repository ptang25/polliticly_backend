<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class poll_likes extends Model
{
    use HasFactory;

    protected $table = 'poll_likes';

    protected $fillable = ['pollID', 'voterID'];

}
