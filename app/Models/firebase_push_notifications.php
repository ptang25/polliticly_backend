<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class firebase_push_notifications extends Model
{
    use HasFactory;

    protected $table = 'firebase_push_notifications';

    protected $fillable = ['voterID', 'token', 'unsubscribe'];


}
