<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class poll_comments extends Model
{
    use HasFactory;

    protected $table = 'poll_comments';

    protected $fillable = ['pollID', 'voterID', 'comment', 'visible'];

}
