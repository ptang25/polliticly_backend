<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class poll_choices extends Model
{
    use HasFactory;

    protected $table = 'poll_choices';

    protected $fillable = ['pollID', 'content'];


}
