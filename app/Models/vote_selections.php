<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class vote_selections extends Model
{
    use HasFactory;

    protected $table = 'vote_selections';

    protected $fillable = ['pollID', 'voterID', 'selection'];

}
