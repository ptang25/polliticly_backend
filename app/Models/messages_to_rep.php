<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class messages_to_rep extends Model
{
    use HasFactory;

    protected $table = 'messages_to_rep';

}
