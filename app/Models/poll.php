<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class poll extends Model
{
    use HasFactory;

    protected $table = 'polls';

    protected $fillable = ['authorID', 'question', 'field', 'num_likes', 'num_comments', 'status', 'hashcode' ];

}
