<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class audit_trail extends Model
{
    use HasFactory;

    protected $table = 'audit_trail';

    protected $fillable = ['voterID', 'table', 'field', 'operation', 'old_value', 'new_value' ];

}
