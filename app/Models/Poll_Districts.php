<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Poll_Districts extends Model
{
    use HasFactory;
    
    protected $table = 'poll_districts';
}
