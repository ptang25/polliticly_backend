<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use RealRashid\SweetAlert\Facades\Alert;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Login Page
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

/*------------------------------------- API Calls -------------------------------------*/
/*--- Voter Helpers  ---*/
Route::post('/validateLogin', [App\Http\Controllers\VoterController::class, 'validate_login']);

/*--- Vote Registration ---*/
//Create or Edit Voter Vitals
Route::post('/editVoter', [App\Http\Controllers\VoterController::class, 'editVoterInfo']);

//Change Password
Route::post('/changeVoterPassword', [App\Http\Controllers\VoterController::class, 'change_voter_password']);

//Set Firebase Token
Route::post('/setPushToken', [App\Http\Controllers\VoterController::class, 'set_notification_token']);

/*--- Voting ---*/
//Create Poll
Route::post('/createAPoll', [App\Http\Controllers\PollController::class, 'createAPoll']);

//Cast Vote
Route::post('/castVote', [App\Http\Controllers\PollController::class, 'castVote']);

//Get Polls
Route::post('/getPolls', [App\Http\Controllers\PollController::class, 'returnPolls']);

//Add Like To Poll
Route::post('/addLike', [App\Http\Controllers\PollController::class, 'addLike']);

//Remove Like from Poll
Route::post('/removeLike', [App\Http\Controllers\PollController::class, 'removeLike']);

//Retrieve Comments from a specific Poll
Route::post('/getComments', [App\Http\Controllers\PollController::class, 'getComments']);

//Add Comment to Poll
Route::post('/addComment', [App\Http\Controllers\PollController::class, 'addComment']);

//Remove Comment to Poll
Route::post('/removeComment', [App\Http\Controllers\PollController::class, 'removeComment']);

/*--- Send Message ---*/
//Send Message
Route::post('/emailReps', [App\Http\Controllers\DistrictController::class, 'sendEmailToReps']);

/*------------------------------------- Admin Panel -------------------------------------*/
/*--- Manage Poll ---*/
Route::get('/managePolls', [App\Http\Controllers\PollController::class, 'showManagePollsView'])->middleware('auth');

//Create a Poll
Route::get('/createPoll', [App\Http\Controllers\PollController::class, 'createPollsView'])->middleware('auth');

//Delete Poll
Route::get('/deletePoll/{pollid}', [App\Http\Controllers\PollController::class, 'deletePoll'])->middleware('auth');

//Archive a Poll
Route::get('/archivePoll/{pollid}', [App\Http\Controllers\PollController::class, 'archivePoll'])->middleware('auth');

//Save Poll to Database
Route::post('/storePoll', [App\Http\Controllers\PollController::class, 'createPoll'])->middleware('auth');

//Update a Poll
Route::get('/updatePoll/{pollid}', [App\Http\Controllers\PollController::class, 'updatePoll'])->middleware('auth');

//Show Polls waiting to be approved
Route::get('/approvePolls', [App\Http\Controllers\PollController::class, 'approvePolls'])->middleware('auth');

//Accept the Poll
Route::get('/acceptPoll/{pollid}', [App\Http\Controllers\PollController::class, 'acceptPoll'])->middleware('auth');

//Reject the Poll
Route::get('/rejectPoll/{pollid}', [App\Http\Controllers\PollController::class, 'rejectPoll'])->middleware('auth');

//Show Polls that have been archived
Route::get('/archivedPolls', [App\Http\Controllers\PollController::class, 'archivedPolls'])->middleware('auth');

//Show Polls that have been rejected
Route::get('/rejectedPolls', [App\Http\Controllers\PollController::class, 'rejectedPolls'])->middleware('auth');

//Get the Links to Poll results
Route::get('/resultsPages/{pollid}', [App\Http\Controllers\PollController::class, 'generateResults'])->middleware('auth');

//Get the Links to Poll results
Route::get('/commentsPage/{pollid}', [App\Http\Controllers\PollController::class, 'comments'])->middleware('auth');

//Get the Links to Poll results
Route::get('/hideComment/{commentid}/{pollid}', [App\Http\Controllers\PollController::class, 'hideComment'])->middleware('auth');

/*--- Manage District ---*/
Route::get('/showDistricts', [App\Http\Controllers\DistrictController::class, 'index'])->middleware('auth');

//Add a Representative
Route::get('/addRep', [App\Http\Controllers\DistrictController::class, 'addRep'])->middleware('auth');

//Edit a Representative
Route::get('/editRep/{repid}', [App\Http\Controllers\DistrictController::class, 'editRep'])->middleware('auth');

//Delete Representative
Route::get('/removeRep/{repid}', [App\Http\Controllers\DistrictController::class, 'removeRep'])->middleware('auth');

//Save Representative to Database
Route::post('/storeRep', [App\Http\Controllers\DistrictController::class, 'createRep'])->middleware('auth');

/*--- Send Push Notification ---*/
//The View that allows the Admin to send a Push Notification
Route::get('/createPN', [App\Http\Controllers\PushNotificationController::class, 'index'])->middleware('auth');

//Sending the Push Notification
Route::post('/sendPN', [App\Http\Controllers\PushNotificationController::class, 'sendPN'])->middleware('auth');

//Unsubscribe for Users
Route::get('/unsubscribe/{email}', [App\Http\Controllers\PushNotificationController::class, 'unsubscribe']);

/*----- Results from Polls ------*/
Route::get('/results/{hashID}', [App\Http\Controllers\PushNotificationController::class, 'unsubscribe']);

//districtType Abbreviations
//city - Entire City
//b - Borough
//a - Assembly
//s - Senate
//c - Congress
//cm - Community
//cc - City Council
Route::get('/poll_results/{hashID}/{districtType}/{districtNum}', [App\Http\Controllers\ChartsController::class, 'testChart']);

/*----- Fix Voter Districts ------*/
Route::get('/fixVoters', [App\Http\Controllers\VoterController::class, 'assembleAddressErrors'])->middleware('auth');

Route::post('/fixVoterDistricts', [App\Http\Controllers\VoterController::class, 'updateVoterDistricts'])->middleware('auth');

Route::get('/addPollDistricts', [App\Http\Controllers\PollController::class, 'setPollDistrictMap'])->middleware('auth');
