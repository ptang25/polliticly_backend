<?php

namespace Database\Seeders;

use App\Models\district;
use Illuminate\Database\Seeder;

class ElectedOfficialsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $assembly = [            
            ['district_num' => 23, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 24, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 25, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 26, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 27, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 28, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 29, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 30, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 32, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 33, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 34, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 35, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 36, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 37, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 38, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 39, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 40, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 41, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 42, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 43, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 44, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 45, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 46, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 47, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 48, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 49, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 50, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 51, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 52, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 53, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 54, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 55, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 56, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 57, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 58, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 59, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 60, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 61, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 62, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 63, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 64, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 65, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 66, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 67, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 68, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 69, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 70, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 71, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 72, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 73, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 74, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 75, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 76, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 77, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 78, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 79, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 80, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 81, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 82, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 83, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 84, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 86, 'district_type' => 'assemblyDistrict'],
            ['district_num' => 87, 'district_type' => 'assemblyDistrict'],
        ];

        foreach ($assembly as $item) {
            district::create($item);
        }

        $senators = [            
            ['district_num' => 10, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 11, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 12, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 13, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 14, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 15, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 16, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 17, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 18, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 19, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 20, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 21, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 22, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 23, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 24, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 25, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 26, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 27, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 28, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 29, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 30, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 31, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 32, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 33, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 34, 'district_type' => 'stateSenatorialDistrict'],
            ['district_num' => 36, 'district_type' => 'stateSenatorialDistrict'],
        ];

        foreach ($senators as $item) {
            district::create($item);
        }

        $citycouncil = [            
            ['district_num' => 1, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 2, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 3, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 4, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 5, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 6, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 7, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 8, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 9, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 10, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 11, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 12, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 13, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 14, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 15, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 16, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 17, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 18, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 19, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 20, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 21, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 22, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 23, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 24, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 25, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 26, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 27, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 28, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 29, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 30, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 31, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 32, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 33, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 34, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 35, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 36, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 37, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 38, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 39, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 40, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 41, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 42, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 43, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 44, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 45, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 46, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 47, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 48, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 49, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 50, 'district_type' => 'cityCouncilDistrict'],
            ['district_num' => 51, 'district_type' => 'cityCouncilDistrict'],
        ];

        foreach ($citycouncil as $item) {
            district::create($item);
        }

        $congress = [            
            ['district_num' => 3, 'district_type' => 'congressionalDistrict'],
            ['district_num' => 5, 'district_type' => 'congressionalDistrict'],
            ['district_num' => 6, 'district_type' => 'congressionalDistrict'],
            ['district_num' => 7, 'district_type' => 'congressionalDistrict'],
            ['district_num' => 8, 'district_type' => 'congressionalDistrict'],
            ['district_num' => 9, 'district_type' => 'congressionalDistrict'],
            ['district_num' => 10, 'district_type' => 'congressionalDistrict'],
            ['district_num' => 11, 'district_type' => 'congressionalDistrict'],
            ['district_num' => 12, 'district_type' => 'congressionalDistrict'],
            ['district_num' => 13, 'district_type' => 'congressionalDistrict'],
            ['district_num' => 14, 'district_type' => 'congressionalDistrict'],
            ['district_num' => 15, 'district_type' => 'congressionalDistrict'],
            ['district_num' => 16, 'district_type' => 'congressionalDistrict'],
        ];

        foreach ($congress as $item) {
            district::create($item);
        }

        $community = [            
            ['district_num' => 101, 'district_type' => 'communityDistrict'],
            ['district_num' => 102, 'district_type' => 'communityDistrict'],
            ['district_num' => 103, 'district_type' => 'communityDistrict'],
            ['district_num' => 104, 'district_type' => 'communityDistrict'],
            ['district_num' => 105, 'district_type' => 'communityDistrict'],
            ['district_num' => 106, 'district_type' => 'communityDistrict'],
            ['district_num' => 107, 'district_type' => 'communityDistrict'],
            ['district_num' => 108, 'district_type' => 'communityDistrict'],
            ['district_num' => 109, 'district_type' => 'communityDistrict'],
            ['district_num' => 110, 'district_type' => 'communityDistrict'],
            ['district_num' => 111, 'district_type' => 'communityDistrict'],
            ['district_num' => 112, 'district_type' => 'communityDistrict'],
            ['district_num' => 201, 'district_type' => 'communityDistrict'],
            ['district_num' => 202, 'district_type' => 'communityDistrict'],
            ['district_num' => 203, 'district_type' => 'communityDistrict'],
            ['district_num' => 204, 'district_type' => 'communityDistrict'],
            ['district_num' => 205, 'district_type' => 'communityDistrict'],
            ['district_num' => 206, 'district_type' => 'communityDistrict'],
            ['district_num' => 207, 'district_type' => 'communityDistrict'],
            ['district_num' => 208, 'district_type' => 'communityDistrict'],
            ['district_num' => 209, 'district_type' => 'communityDistrict'],
            ['district_num' => 210, 'district_type' => 'communityDistrict'],
            ['district_num' => 211, 'district_type' => 'communityDistrict'],
            ['district_num' => 212, 'district_type' => 'communityDistrict'],
            ['district_num' => 301, 'district_type' => 'communityDistrict'],
            ['district_num' => 302, 'district_type' => 'communityDistrict'],
            ['district_num' => 303, 'district_type' => 'communityDistrict'],
            ['district_num' => 304, 'district_type' => 'communityDistrict'],
            ['district_num' => 305, 'district_type' => 'communityDistrict'],
            ['district_num' => 306, 'district_type' => 'communityDistrict'],
            ['district_num' => 307, 'district_type' => 'communityDistrict'],
            ['district_num' => 308, 'district_type' => 'communityDistrict'],
            ['district_num' => 309, 'district_type' => 'communityDistrict'],
            ['district_num' => 310, 'district_type' => 'communityDistrict'],
            ['district_num' => 311, 'district_type' => 'communityDistrict'],
            ['district_num' => 312, 'district_type' => 'communityDistrict'],
            ['district_num' => 313, 'district_type' => 'communityDistrict'],
            ['district_num' => 314, 'district_type' => 'communityDistrict'],
            ['district_num' => 315, 'district_type' => 'communityDistrict'],
            ['district_num' => 316, 'district_type' => 'communityDistrict'],
            ['district_num' => 317, 'district_type' => 'communityDistrict'],
            ['district_num' => 318, 'district_type' => 'communityDistrict'],
            ['district_num' => 401, 'district_type' => 'communityDistrict'],
            ['district_num' => 402, 'district_type' => 'communityDistrict'],
            ['district_num' => 403, 'district_type' => 'communityDistrict'],
            ['district_num' => 404, 'district_type' => 'communityDistrict'],
            ['district_num' => 405, 'district_type' => 'communityDistrict'],
            ['district_num' => 406, 'district_type' => 'communityDistrict'],
            ['district_num' => 407, 'district_type' => 'communityDistrict'],
            ['district_num' => 408, 'district_type' => 'communityDistrict'],
            ['district_num' => 409, 'district_type' => 'communityDistrict'],
            ['district_num' => 410, 'district_type' => 'communityDistrict'],
            ['district_num' => 411, 'district_type' => 'communityDistrict'],
            ['district_num' => 412, 'district_type' => 'communityDistrict'],
            ['district_num' => 413, 'district_type' => 'communityDistrict'],
            ['district_num' => 414, 'district_type' => 'communityDistrict'],
            ['district_num' => 501, 'district_type' => 'communityDistrict'],
            ['district_num' => 502, 'district_type' => 'communityDistrict'],
            ['district_num' => 503, 'district_type' => 'communityDistrict'],
        ];

        foreach ($community as $item) {
            district::create($item);
        }
    }
}
