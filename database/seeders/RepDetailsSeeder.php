<?php

namespace Database\Seeders;

use App\Models\district_rep;
use Illuminate\Database\Seeder;

class RepDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $assemblymembers = [            
            ['districtID' => 1, 'rep_name' => 'Stacey Pheffer Amato', 'rep_email' => 'amatos@nyassembly.gov'],
            ['districtID' => 2, 'rep_name' => 'David Weprin', 'rep_email' => 'WeprinD@nyassembly.gov'],
            ['districtID' => 3, 'rep_name' => 'Nily Rozic', 'rep_email' => 'RozicN@nyassembly.gov'],
            ['districtID' => 4, 'rep_name' => 'Edward Braunstein', 'rep_email' => 'braunsteine@nyassembly.gov'],
            ['districtID' => 5, 'rep_name' => 'Daniel Rosenthal', 'rep_email' => 'rosenthald@nyassembly.gov'],
            ['districtID' => 6, 'rep_name' => 'Andrew Hevesi', 'rep_email' => 'HevesiA@nyassembly.gov'],
            ['districtID' => 7, 'rep_name' => 'Alicia Hyndman', 'rep_email' => 'hyndmana@nyassembly.gov'],
            ['districtID' => 8, 'rep_name' => 'Brian Barnwell', 'rep_email' => 'BarnwellB@nyassembly.gov'],
            ['districtID' => 9, 'rep_name' => 'Vivian E. Cook', 'rep_email' => 'CookV@nyassembly.gov'],
            ['districtID' => 10, 'rep_name' => 'Clyde Vanel', 'rep_email' => 'vanelc@nyassembly.gov'],
            ['districtID' => 11, 'rep_name' => 'Michael DenDekker', 'rep_email' => 'DenDekkerM@nyassembly.gov'],
            ['districtID' => 12, 'rep_name' => 'Jeffrion L. Aubry', 'rep_email' => 'AubryJ@nyassembly.gov'],
            ['districtID' => 13, 'rep_name' => 'Aravella Simotas', 'rep_email' => 'simotasa@nyassembly.gov'],
            ['districtID' => 14, 'rep_name' => 'Catherine Nolan', 'rep_email' => 'NolanC@nyassembly.gov'],
            ['districtID' => 15, 'rep_name' => 'Michael G. Miller', 'rep_email' => 'MillerMG@nyassembly.gov'],
            ['districtID' => 16, 'rep_name' => 'Catalina Cruz', 'rep_email' => 'cruzc@nyassembly.gov'],
            ['districtID' => 17, 'rep_name' => 'Ron Kim', 'rep_email' => 'KimR@nyassembly.gov'],
            ['districtID' => 18, 'rep_name' => 'Helene Weinstein', 'rep_email' => 'WeinstH@nyassembly.gov'],
            ['districtID' => 19, 'rep_name' => 'Rodneyse Bichotte', 'rep_email' => 'bichotter@nyassembly.gov'],
            ['districtID' => 20, 'rep_name' => 'Diana Richardson', 'rep_email' => 'district43@nyassembly.gov'],
            ['districtID' => 21, 'rep_name' => 'Robert Carroll', 'rep_email' => 'CarrollR@nyassembly.gov'],
            ['districtID' => 22, 'rep_name' => 'Steven Cymbrowitz', 'rep_email' => 'CymbroS@nyassembly.gov'],
            ['districtID' => 23, 'rep_name' => 'Mathylde Frontus', 'rep_email' => 'FrontusM@nyassembly.gov'],
            ['districtID' => 24, 'rep_name' => 'William Colton', 'rep_email' => 'ColtonW@nyassembly.gov'],
            ['districtID' => 25, 'rep_name' => 'Simcha Eichenstein', 'rep_email' => 'EichensteinS@nyassembly.gov'],
            ['districtID' => 26, 'rep_name' => 'Peter J. Abbate Jr.', 'rep_email' => 'abbatep@nyassembly.gov'],
            ['districtID' => 27, 'rep_name' => 'Joseph R. Lentol', 'rep_email' => 'LentolJ@nyassembly.gov'],
            ['districtID' => 28, 'rep_name' => 'Félix W. Ortiz', 'rep_email' => 'OrtizF@nyassembly.gov'],
            ['districtID' => 29, 'rep_name' => 'Jo Anne Simon', 'rep_email' => 'simonj@nyassembly.gov'],
            ['districtID' => 30, 'rep_name' => 'Maritza Davila', 'rep_email' => 'avilaM@nyassembly.gov'],
            ['districtID' => 31, 'rep_name' => 'Erik Martin Dilan', 'rep_email' => 'DilanE@nyassembly.gov'],
            ['districtID' => 32, 'rep_name' => 'Latrice Walker', 'rep_email' => 'WalkerL@nyassembly.gov'],
            ['districtID' => 33, 'rep_name' => 'Tremaine Wright', 'rep_email' => 'wrightt@nyassembly.gov'],
            ['districtID' => 34, 'rep_name' => 'Walter T. Mosley', 'rep_email' => 'MosleyW@nyassembly.gov'],
            ['districtID' => 35, 'rep_name' => 'N. Nick Perry', 'rep_email' => 'PerryN@nyassembly.gov'],
            ['districtID' => 36, 'rep_name' => 'Jaime Williams', 'rep_email' => 'williamsja@nyassembly.gov'],
            ['districtID' => 37, 'rep_name' => 'Charles Barron', 'rep_email' => 'barronc@nyassembly.gov'],
            ['districtID' => 38, 'rep_name' => 'Charles Fall', 'rep_email' => 'fallc@nyassembly.gov'],
            ['districtID' => 39, 'rep_name' => 'Michael Reilly', 'rep_email' => 'reillym@nyassembly.gov'],
            ['districtID' => 40, 'rep_name' => 'Michael Cusick', 'rep_email' => 'CusickM@nyassembly.gov'],
            ['districtID' => 41, 'rep_name' => 'Nicole Malliotakis', 'rep_email' => 'malliotakisn@nyassembly.gov'],
            ['districtID' => 42, 'rep_name' => 'Yuh-Line Niou', 'rep_email' => 'niouy@nyassembly.gov'],
            ['districtID' => 43, 'rep_name' => 'Deborah J. Glick', 'rep_email' => 'GlickD@nyassembly.gov'],
            ['districtID' => 44, 'rep_name' => 'Linda Rosenthal', 'rep_email' => 'RosentL@nyassembly.gov'],
            ['districtID' => 45, 'rep_name' => 'Robert J. Rodriguez', 'rep_email' => 'Rrodriguez@nyassembly.gov'],
            ['districtID' => 46, 'rep_name' => 'Daniel J. O\'Donnell', 'rep_email' => 'OdonnellD@nyassembly.gov'],
            ['districtID' => 47, 'rep_name' => 'Inez Dickens', 'rep_email' => 'DickensI@nyassembly.gov'],
            ['districtID' => 48, 'rep_name' => 'Al Taylor', 'rep_email' => 'taylora@nyassembly.gov'],
            ['districtID' => 49, 'rep_name' => 'Carmen De La Rosa', 'rep_email' => 'delaRosac@nyassembly.gov'],
            ['districtID' => 50, 'rep_name' => 'Dan Quart', 'rep_email' => 'quartd@nyassembly.gov'],
            ['districtID' => 51, 'rep_name' => 'Harvey Epstein', 'rep_email' => 'epsteinh@nyassembly.gov'],
            ['districtID' => 52, 'rep_name' => 'Richard N. Gottfried', 'rep_email' => 'GottfriedR@nyassembly.gov'],
            ['districtID' => 53, 'rep_name' => 'Rebecca Seawright', 'rep_email' => 'SeawrightR@nyassembly.gov'],
            ['districtID' => 54, 'rep_name' => 'Latoya Joyner', 'rep_email' => 'joynerl@nyassembly.gov'],
            ['districtID' => 55, 'rep_name' => 'Jose Rivera', 'rep_email' => 'RiveraJ@nyassembly.gov'],
            ['districtID' => 56, 'rep_name' => 'Michael Blake', 'rep_email' => 'BlakeM@nyassembly.gov'],
            ['districtID' => 57, 'rep_name' => 'Nathalia Fernandez', 'rep_email' => 'fernandezn@nyassembly.gov'],
            ['districtID' => 58, 'rep_name' => 'Jeffrey Dinowitz', 'rep_email' => 'DinowiJ@nyassembly.gov'],
            ['districtID' => 59, 'rep_name' => 'Michael Benedetto', 'rep_email' => 'benedettom@nyassembly.gov'],
            ['districtID' => 60, 'rep_name' => 'Carl Heastie', 'rep_email' => 'Speaker@nyassembly.gov'],
            ['districtID' => 61, 'rep_name' => 'Carmen E. Arroyo', 'rep_email' => 'ArroyoC@nyassembly.gov'],
            ['districtID' => 62, 'rep_name' => 'Victor M. Pichardo', 'rep_email' => 'PichardoV@nyassembly.gov'],
            ['districtID' => 63, 'rep_name' => 'Karines Reyes', 'rep_email' => 'reyesk@nyassembly.gov'],
            
        ];

        foreach ($assemblymembers as $item) {
            district_rep::create($item);
        }

        $senatorsmembers = [           
            ['districtID' => 64, 'rep_name' => 'James Sanders, Jr.', 'rep_email' => 'sanders@nysenate.gov'],
            ['districtID' => 65, 'rep_name' => 'John Liu', 'rep_email' => 'liu@nysenate.gov'],
            ['districtID' => 66, 'rep_name' => 'Michael Gianaris', 'rep_email' => 'gianaris@nysenate.gov'],
            ['districtID' => 67, 'rep_name' => 'Jessica Ramos', 'rep_email' => 'ramos@nysenate.gov'],
            ['districtID' => 68, 'rep_name' => 'Leroy Comrie', 'rep_email' => 'comrie@nysenate.gov'],
            ['districtID' => 69, 'rep_name' => 'Joseph Addabbo', 'rep_email' => 'addabbo@nysenate.gov'],
            ['districtID' => 70, 'rep_name' => 'Toby Ann Stavisky', 'rep_email' => 'stavisky@nysenate.gov'],
            ['districtID' => 71, 'rep_name' => 'Simcha Felder', 'rep_email' => 'felder@nysenate.gov'],
            ['districtID' => 72, 'rep_name' => 'Julia Salazar', 'rep_email' => 'salazar@nysenate.gov'],
            ['districtID' => 73, 'rep_name' => 'Roxanne Persaud', 'rep_email' => 'senatorpersaud@gmail.com'],
            ['districtID' => 74, 'rep_name' => 'Zellnor Myrie', 'rep_email' => 'myrie@nysenate.gov'],
            ['districtID' => 75, 'rep_name' => 'Kevin Parker', 'rep_email' => 'parker@nysenate.gov'],
            ['districtID' => 76, 'rep_name' => 'Andrew Gounardes', 'rep_email' => 'gounardes@nysenate.gov'],
            ['districtID' => 77, 'rep_name' => 'Diane Savino', 'rep_email' => 'Savino@nysenate.gov'],
            ['districtID' => 78, 'rep_name' => 'Andrew Lanza', 'rep_email' => 'lanza@nysenate.gov'],
            ['districtID' => 79, 'rep_name' => 'Velmanette Montgomery', 'rep_email' => 'montgome@nysenate.gov'],
            ['districtID' => 80, 'rep_name' => 'Brian Kavanagh', 'rep_email' => 'kavanagh@nysenate.gov'],
            ['districtID' => 81, 'rep_name' => 'Brad Hoylman', 'rep_email' => 'hoylman@nysenate.gov'],
            ['districtID' => 82, 'rep_name' => 'Liz Krueger	', 'rep_email' => 'lkrueger@nysenate.gov'],
            ['districtID' => 83, 'rep_name' => 'Jose M. Serrano', 'rep_email' => 'serrano@nysenate.gov'],
            ['districtID' => 84, 'rep_name' => 'Brian Benjamin', 'rep_email' => 'bbenjamin@nysenate.gov'],
            ['districtID' => 85, 'rep_name' => 'Robert Jackson', 'rep_email' => 'jackson@nysenate.gov'],
            ['districtID' => 86, 'rep_name' => 'Luis Sepulveda', 'rep_email' => 'sepulveda@nysenate.gov'],
            ['districtID' => 87, 'rep_name' => 'J. Gustavo Rivera', 'rep_email' => 'grivera@nysenate.gov'],
            ['districtID' => 88, 'rep_name' => 'Alessandra Biaggi', 'rep_email' => 'biaggi@nysenate.gov'],
            ['districtID' => 89, 'rep_name' => 'Jamaal Bailey', 'rep_email' => 'bailey@nysenate.gov'],
        
        ];    
        
        foreach ($senatorsmembers as $item) {
                district_rep::create($item);
        }
        
        $citycouncilmembers = [            
            ['districtID' => 90, 'rep_name' => 'Margaret Chin', 'borough' => 'Manhattan', 'rep_email' => 'chin@council.nyc.gov'],
            ['districtID' => 91, 'rep_name' => 'Carlina Rivera', 'borough' => 'Manhattan', 'rep_email' => 'District2@council.nyc.gov'],
            ['districtID' => 92, 'rep_name' => 'Corey Johnson', 'borough' => 'Manhattan', 'rep_email' => 'SpeakerJohnson@council.nyc.gov'],
            ['districtID' => 93, 'rep_name' => 'Keith Powers', 'borough' => 'Manhattan', 'rep_email' => 'KPowers@council.nyc.gov'],
            ['districtID' => 94, 'rep_name' => 'Ben Kallos', 'borough' => 'Manhattan', 'rep_email' => 'BKallos@BenKallos.com'],
            ['districtID' => 95, 'rep_name' => 'Helen Rosenthal', 'borough' => 'Manhattan', 'rep_email' => 'Helen@HelenRosenthal.com'],
            ['districtID' => 96, 'rep_name' => 'Mark Levine', 'borough' => 'Manhattan', 'rep_email' => 'District7@council.nyc.gov'],
            ['districtID' => 97, 'rep_name' => 'Diana Ayala', 'borough' => 'Manhattan and Bronx', 'rep_email' => 'DAyala@council.nyc.gov'],
            ['districtID' => 98, 'rep_name' => 'Bill Perkins', 'borough' => 'Manhattan', 'rep_email' => 'D09perkins@council.nyc.gov'],
            ['districtID' => 99, 'rep_name' => 'Ydanis Rodriguez', 'borough' => 'Manhattan', 'rep_email' => 'yrodriguez@council.nyc.gov'],
            ['districtID' => 100, 'rep_name' => 'Andrew Cohen', 'borough' => 'Bronx', 'rep_email' => 'District11@council.nyc.gov'],
            ['districtID' => 101, 'rep_name' => 'Andy King', 'borough' => 'Bronx', 'rep_email' => 'Andy.King@council.nyc.gov'],
            ['districtID' => 102, 'rep_name' => 'Mark Gjonaj', 'borough' => 'Bronx', 'rep_email' => 'MGjonaj@council.nyc.gov'],
            ['districtID' => 103, 'rep_name' => 'Fernando Cabrera', 'borough' => 'Bronx', 'rep_email' => 'fcabrera@council.nyc.gov'],
            ['districtID' => 104, 'rep_name' => 'Ritchie Torres', 'borough' => 'Bronx', 'rep_email' => 'Rtorres@council.nyc.gov'],
            ['districtID' => 105, 'rep_name' => 'Vanessa L Gibson', 'borough' => 'Bronx', 'rep_email' => 'District16Bronx@council.nyc.gov'],
            ['districtID' => 106, 'rep_name' => 'Rafael Salamanca', 'borough' => 'Bronx', 'rep_email' => 'salamanca@council.nyc.gov'],
            ['districtID' => 107, 'rep_name' => 'Ruben Diaz, Sr.', 'borough' => 'Bronx', 'rep_email' => 'RDiaz@council.nyc.gov'],
            ['districtID' => 108, 'rep_name' => 'Paul Vallone', 'borough' => 'Queens', 'rep_email' => 'district19@council.nyc.gov'],
            ['districtID' => 109, 'rep_name' => 'Peter Koo', 'borough' => 'Queens', 'rep_email' => 'pkoo@council.nyc.gov'],
            ['districtID' => 110, 'rep_name' => 'Francisco Moya', 'borough' => 'Queens', 'rep_email' => 'FMoya@council.nyc.gov'],
            ['districtID' => 111, 'rep_name' => 'Costa Constantinides', 'borough' => 'Queens', 'rep_email' => ''],
            ['districtID' => 112, 'rep_name' => 'Barry Grodenchik', 'borough' => 'Queens', 'rep_email' => 'BGrodenchik@council.nyc.gov'],
            ['districtID' => 113, 'rep_name' => 'Rory Lancman', 'borough' => 'Queens', 'rep_email' => 'RLancman@council.nyc.gov'],
            ['districtID' => 114, 'rep_name' => 'Daniel Dromm', 'borough' => 'Queens', 'rep_email' => 'dromm@council.nyc.gov'],
            ['districtID' => 115, 'rep_name' => 'Jimmy Van Bramer', 'borough' => 'Queens', 'rep_email' => 'JVanBramer@council.nyc.gov'],
            ['districtID' => 116, 'rep_name' => 'I. Daneek Miller', 'borough' => 'Queens', 'rep_email' => ''],
            ['districtID' => 117, 'rep_name' => 'Adrienne Adams', 'borough' => 'Queens', 'rep_email' => 'Adams@council.nyc.gov'],
            ['districtID' => 118, 'rep_name' => 'Karen Koslowitz', 'borough' => 'Queens', 'rep_email' => 'Koslowitz@council.nyc.gov'],
            ['districtID' => 119, 'rep_name' => 'Robert Holden', 'borough' => 'Queens', 'rep_email' => 'District30@council.nyc.gov'],
            ['districtID' => 120, 'rep_name' => 'Donovan Richards', 'borough' => 'Queens', 'rep_email' => 'drichards@council.nyc.gov'],
            ['districtID' => 121, 'rep_name' => 'Eric Ulrich', 'borough' => 'Queens', 'rep_email' => 'eulrich@council.nyc.gov'],
            ['districtID' => 122, 'rep_name' => 'Stephen Levin', 'borough' => 'Brooklyn', 'rep_email' => 'slevin@council.nyc.gov'],
            ['districtID' => 123, 'rep_name' => 'Antonio Reynoso', 'borough' => 'Brooklyn and Queens', 'rep_email' => 'areynoso@council.nyc.gov'],
            ['districtID' => 124, 'rep_name' => 'Laurie Cumbo', 'borough' => 'Brooklyn', 'rep_email' => 'LCumbo@council.nyc.gov'],
            ['districtID' => 125, 'rep_name' => 'Robert Cornegy', 'borough' => 'Brooklyn', 'rep_email' => 'district36@council.nyc.gov'],
            ['districtID' => 126, 'rep_name' => 'Rafael Espinal', 'borough' => 'Brooklyn', 'rep_email' => ''],
            ['districtID' => 127, 'rep_name' => 'Carlos Menchaca', 'borough' => 'Brooklyn', 'rep_email' => 'info38@council.nyc.gov'],
            ['districtID' => 128, 'rep_name' => 'Brad Lander', 'borough' => 'Brooklyn', 'rep_email' => 'lander@council.nyc.gov'],
            ['districtID' => 129, 'rep_name' => 'Mathieu Eugene', 'borough' => 'Brooklyn', 'rep_email' => 'meugene@council.nyc.gov'],
            ['districtID' => 130, 'rep_name' => 'Alicka Ampry-Samuel', 'borough' => 'Brooklyn', 'rep_email' => 'District41@council.nyc.gov'],
            ['districtID' => 131, 'rep_name' => 'Inez Barron', 'borough' => 'Brooklyn', 'rep_email' => ''],
            ['districtID' => 132, 'rep_name' => 'Justin Brannan', 'borough' => 'Brooklyn', 'rep_email' => 'AskJB@council.nyc.gov'],
            ['districtID' => 133, 'rep_name' => 'Kalman Yeger', 'borough' => 'Brooklyn', 'rep_email' => 'AskKalman@council.nyc.gov'],
            ['districtID' => 134, 'rep_name' => 'Farah Louis', 'borough' => 'Brooklyn', 'rep_email' => 'District45@council.nyc.gov'],
            ['districtID' => 135, 'rep_name' => 'Alan Maisel', 'borough' => 'Brooklyn', 'rep_email' => 'AMaisel@council.nyc.gov'],
            ['districtID' => 136, 'rep_name' => 'Mark Treyger', 'borough' => 'Brooklyn', 'rep_email' => 'MTreyger@council.nyc.gov'],
            ['districtID' => 137, 'rep_name' => 'Chaim M. Deutsch', 'borough' => 'Brooklyn', 'rep_email' => 'cdeutsch@council.nyc.gov'],
            ['districtID' => 138, 'rep_name' => 'Deborah Rose', 'borough' => 'Staten Island', 'rep_email' => 'DROSE@Council.nyc.gov'],
            ['districtID' => 139, 'rep_name' => 'Steven Matteo', 'borough' => 'Staten Island', 'rep_email' => 'SMatteo@council.nyc.gov'],
            ['districtID' => 140, 'rep_name' => 'Joseph Borelli', 'borough' => 'Staten Island', 'rep_email' => 'borelli@council.nyc.gov'],
        ];

        foreach ($citycouncilmembers as $item) {
            district_rep::create($item);
        }

        $congressmembers = [            
            ['districtID' => 141, 'rep_name' => 'Thomas Suozzi', 'rep_email' => ''],
            ['districtID' => 142, 'rep_name' => 'Gregory Meeks', 'rep_email' => ''],
            ['districtID' => 143, 'rep_name' => 'Grace Meng', 'rep_email' => ''],
            ['districtID' => 144, 'rep_name' => 'Nydia Velázquez', 'rep_email' => ''],
            ['districtID' => 145, 'rep_name' => 'Hakeem Jeffries', 'rep_email' => ''],
            ['districtID' => 146, 'rep_name' => 'Yvette Clarke', 'rep_email' => ''],
            ['districtID' => 147, 'rep_name' => 'Jerrold Nadler', 'rep_email' => ''],
            ['districtID' => 148, 'rep_name' => 'Max Rose', 'rep_email' => ''],
            ['districtID' => 149, 'rep_name' => 'Carolyn Maloney', 'rep_email' => ''],
            ['districtID' => 150, 'rep_name' => 'Adriano Espaillat', 'rep_email' => ''],
            ['districtID' => 151, 'rep_name' => 'Alexandria Ocasio-Cortez', 'rep_email' => ''],
            ['districtID' => 152, 'rep_name' => 'José Serrano', 'rep_email' => ''],
            ['districtID' => 153, 'rep_name' => 'Eliot Engel', 'rep_email' => ''],
        ];

        foreach ($congressmembers as $item) {
            district_rep::create($item);
        }

        $communitymembers = [    
            //Manhattan        
            ['districtID' => 154, 'rep_name' => '', 'rep_email' => 'man01@cb.nyc.gov'], 
            ['districtID' => 155, 'rep_name' => '', 'rep_email' => 'bgormley@cb.nyc.gov'],  
            ['districtID' => 156, 'rep_name' => '', 'rep_email' => 'mn03@cb.nyc.gov'],        
            ['districtID' => 157, 'rep_name' => '', 'rep_email' => 'jbodine@cb.nyc.gov'],     
            ['districtID' => 158, 'rep_name' => '', 'rep_email' => 'office@cb5.org'],     
            ['districtID' => 159, 'rep_name' => '', 'rep_email' => 'info@cbsix.org'],     
            ['districtID' => 160, 'rep_name' => '', 'rep_email' => 'office@cb7.org'],     
            ['districtID' => 161, 'rep_name' => '', 'rep_email' => 'info@cb8m.com'],     
            ['districtID' => 162, 'rep_name' => '', 'rep_email' => 'nyc-cb9m@juno.com'],     
            ['districtID' => 163, 'rep_name' => '', 'rep_email' => 'mn10@cb.nyc.gov'],     
            ['districtID' => 164, 'rep_name' => '', 'rep_email' => 'mn11@cb.nyc.gov'],     
            ['districtID' => 165, 'rep_name' => '', 'rep_email' => 'DBlow@cb.nyc.gov'], 
            //Bronx
            ['districtID' => 166, 'rep_name' => '', 'rep_email' => 'brxcb1@optonline.net'], 
            ['districtID' => 167, 'rep_name' => '', 'rep_email' => 'brxcb2@optonline.net'],  
            ['districtID' => 168, 'rep_name' => '', 'rep_email' => 'brxcomm3@optonline.net'],        
            ['districtID' => 169, 'rep_name' => '', 'rep_email' => 'bx04@cb.nyc.gov'],     
            ['districtID' => 170, 'rep_name' => '', 'rep_email' => 'brxcb5@optonline.net'],     
            ['districtID' => 171, 'rep_name' => '', 'rep_email' => 'bronxcb6@bronxcb6.org'],     
            ['districtID' => 172, 'rep_name' => '', 'rep_email' => 'ibravo@cb.nyc.gov'],     
            ['districtID' => 173, 'rep_name' => '', 'rep_email' => 'bx08@cb.nyc.gov'],     
            ['districtID' => 174, 'rep_name' => '', 'rep_email' => 'bxbrd09@optonline.net'],     
            ['districtID' => 175, 'rep_name' => '', 'rep_email' => 'bx10@cb.nyc.gov'],     
            ['districtID' => 176, 'rep_name' => '', 'rep_email' => 'aldangelo@cb.nyc.gov'],     
            ['districtID' => 177, 'rep_name' => '', 'rep_email' => 'gtorres@cb.nyc.gov'], 
            //Brooklyn
            ['districtID' => 178, 'rep_name' => '', 'rep_email' => 'bk01@cb.nyc.gov'], 
            ['districtID' => 179, 'rep_name' => '', 'rep_email' => 'bk02@cb.nyc.gov'],  
            ['districtID' => 180, 'rep_name' => '', 'rep_email' => 'bk03@cb.nyc.gov'],        
            ['districtID' => 181, 'rep_name' => '', 'rep_email' => 'bk04@cb.nyc.gov'],     
            ['districtID' => 182, 'rep_name' => '', 'rep_email' => 'bk05@cb.nyc.gov'],     
            ['districtID' => 183, 'rep_name' => '', 'rep_email' => 'info@brooklyncb6.org'],     
            ['districtID' => 184, 'rep_name' => '', 'rep_email' => 'bk07@cb.nyc.gov'],     
            ['districtID' => 185, 'rep_name' => '', 'rep_email' => 'brooklyncb8@gmail.com'],     
            ['districtID' => 186, 'rep_name' => '', 'rep_email' => 'bk09@cb.nyc.gov'],     
            ['districtID' => 187, 'rep_name' => '', 'rep_email' => 'bk10@cb.nyc.gov'],     
            ['districtID' => 188, 'rep_name' => '', 'rep_email' => 'info@brooklyncb11.org'],     
            ['districtID' => 189, 'rep_name' => '', 'rep_email' => 'BKCB12@gmail.com'], 
            ['districtID' => 190, 'rep_name' => '', 'rep_email' => 'EDMARK@CB.NYC.GOV'], 
            ['districtID' => 191, 'rep_name' => '', 'rep_email' => 'info@cb14brooklyn.com'], 
            ['districtID' => 192, 'rep_name' => '', 'rep_email' => 'bklcb15@verizon.net'], 
            ['districtID' => 193, 'rep_name' => '', 'rep_email' => 'bk16@cb.nyc.gov'], 
            ['districtID' => 194, 'rep_name' => '', 'rep_email' => 'bk17@cb.nyc.gov'], 
            ['districtID' => 195, 'rep_name' => '', 'rep_email' => 'bkbrd18@optonline.net'],
            //Queens
            ['districtID' => 196, 'rep_name' => '', 'rep_email' => 'qn01@cb.nyc.gov'], 
            ['districtID' => 197, 'rep_name' => '', 'rep_email' => 'qn02@cb.nyc.gov'],
            ['districtID' => 198, 'rep_name' => '', 'rep_email' => 'communityboard3@nyc.rr.com'],
            ['districtID' => 199, 'rep_name' => '', 'rep_email' => 'qn04@cb.nyc.gov'],
            ['districtID' => 200, 'rep_name' => '', 'rep_email' => 'qn05@cb.nyc.gov'],
            ['districtID' => 201, 'rep_name' => '', 'rep_email' => 'qn06@cb.nyc.gov'],
            ['districtID' => 202, 'rep_name' => '', 'rep_email' => 'qn07@cb.nyc.gov'],
            ['districtID' => 203, 'rep_name' => '', 'rep_email' => 'qn08@cb.nyc.gov'],
            ['districtID' => 204, 'rep_name' => '', 'rep_email' => 'communitybd9@nyc.rr.com'],
            ['districtID' => 205, 'rep_name' => '', 'rep_email' => 'qn10@cb.nyc.gov'],
            ['districtID' => 206, 'rep_name' => '', 'rep_email' => 'qn11@cb.nyc.gov'],
            ['districtID' => 207, 'rep_name' => '', 'rep_email' => 'qn12@cb.nyc.gov'],
            ['districtID' => 208, 'rep_name' => '', 'rep_email' => 'mmcmillan@qcb13.org'],
            ['districtID' => 209, 'rep_name' => '', 'rep_email' => 'cbrock14@nyc.rr.com'],
            //Staten Island
            ['districtID' => 210, 'rep_name' => '', 'rep_email' => 'sicb1@si.rr.com'],
            ['districtID' => 211, 'rep_name' => '', 'rep_email' => 'dderrico@cb.nyc.gov'],
            ['districtID' => 212, 'rep_name' => '', 'rep_email' => 'sicb3@cb.nyc.gov'],
        ];

        foreach ($communitymembers as $item) {
            district_rep::create($item);
        }
    }
}
