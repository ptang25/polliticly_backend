<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistrictRepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('district_rep', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('districtID');
            $table->string('borough');
            $table->string('rep_name');
            $table->string('rep_email');
            $table->string('rep_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('district_rep');
    }
}
