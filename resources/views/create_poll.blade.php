@extends('layouts.app')

@section('content')
<div class="wrapper">
    <h4 class="centertext">Create a Poll</h4>
    <form method="POST" enctype="multipart/form-data" id="matchup_form" action="/storePoll"> 
        @csrf
        @isset($poll)
            <input type="hidden" id="pollID" name="pollID" value="{{ $poll->id }}">
        @endisset

        <div class="row justify-content-center">
            <label for="date">Is Poll Visible to Public?</label>
        </div>
        <div class="row justify-content-center">
            <!-- Rounded switch -->
            <label class="switch">
                <input type="checkbox" name="status" value="true" 
                    @isset($poll)
                        @if ($poll->status == 1)
                            checked="checked"
                        @endif
                    @endisset
                >
                <span class="slider round"></span>
            </label>
        </div>

        <!-- Select Which District can Vote -->
        <div class="row justify-content-center">
            <label for="choice" id="choice">Eligible Voter Districts</label>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-2 col-xs-5 ml-2">
                <select id="district_type" name="district_type" class="selectComponentReg center" onChange="type_changed(this);">
                        <option value='city' selected='true'>City</option>
                        <option value='boroughs'>Boroughs</option>
                        <option value='assemblyDistrict'>Assembly</option>
                        <option value='citycouncilDistrict'>City Council</option>
                        <option value='communityDistrict'>Community</option>
                        <option value='congressDistrict'>Congress</option>
                        <option value='stateSenatorialDistrict'>Senate</option>
                </select>
            </div>
    
            <div class="col-md-2 col-xs-5">
                <select id="district_num" name="district_num" class="selectComponentReg center" onChange="districtnum_changed(this);">
                    @foreach ($city as $district)
                        <option value='{{ urlencode($district) }}'>{{ $district }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-1 col-sm-5">
                <span class="fas fa-plus-circle center add-district-button" id="add_districts" type="button" onclick="add_districts()"> Add</span>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class='col-sm-5'>
                <div id="show_districts" >
                    @isset($poll_district)
                        @foreach ($poll_district as $district)
                            <div class="row" id="{{ substr($district->district_type, 0, 2) . "_" . $district->district_num }}">
                                <div class="col-md-10 col-xs-8">
                                    <input type="text" class="district_label" id="poll_districts[]" name="poll_districts[]" value="{{ $district->district_type }}:{{ $district->district_num }}" readonly="readonly"/>
                                </div>
                                <span class="col-md-2 col-xs-4 fas fa-minus-circle center remove_district" onclick="remove_district('{{ substr($district->district_type, 0, 2) . "_" . $district->district_num }}')"></span>
                            </div>
                        @endforeach
                    @endisset
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <label for="date">Poll Sponsor:</label>
        </div>
        <div class="row justify-content-center">
            <div class='col-sm-5'>
                <textarea class="form-control" id="poll_sponsor" name="poll_sponsor" placeholder="Who is sponsoring this post (if anyone)">@isset($poll){{ $poll->sponsor }}@endisset
                </textarea>
            </div>
        </div>


        <div class="row justify-content-center">
            <label for="date">Poll Question:</label>
        </div>
        <div class="row justify-content-center">
            <div class='col-sm-5'>
                <textarea class="form-control" id="poll_question" name="poll_question" placeholder="Type your question here">@isset($poll){{ $poll->question }}@endisset
                </textarea>
            </div>
        </div>

        
        
       
        <div class="row justify-content-center">
            <label for="choice" id="choice">Choice</label>
        </div>

        <div id='poll_selection_add'>

            @isset($choices)
                @foreach ($choices as $choice)
                    <div class="row justify-content-center">
                        <div class='col-sm-5'>
                                    <input type="text" id="poll_choices[]" name="poll_choices[]" placeholder="Enter response" 
                                        @isset($choice)
                                            value="{{ $choice->content }}"
                                        @endisset
                                            class="border-bottom-input">
                        </div>
                    </div>
                @endforeach
            @endisset

        </div>

        <div class='row justify-content-center'>
            <div class='col-sm-5'>
                <button class="fas fa-plus-circle fa-2x center add-choice-button" id="addresponse" type="button" onclick="add()">  Add Choice</button>
            </div>
        </div>

        <div class="row justify-content-center">
            <label for="date">Additional Information:</label>
        </div>
        <div class="row justify-content-center">
            <div class='col-sm-5'>
                <textarea class="form-control" id="poll_more" name="poll_more" placeholder="Additional Information" rows="10">@isset($poll){{ $poll->more }}@endisset
                </textarea>
            </div>
        </div>

        <div class="row justify-content-center">
            <label for="date">External Link:</label>
        </div>
        <div class="row justify-content-center">
            <div class='col-sm-5'>
                <textarea class="form-control" id="poll_link" name="poll_link" placeholder="External Link">@isset($poll){{ $poll->link }}@endisset
                </textarea>
            </div>
        </div>

        <div class="row justify-content-center">
            <input type="submit" value=
                @isset($poll)
                    "Update Poll"
                @else
                    "Create Poll"
                @endisset 
            onclick="check_errors(event)">
        </div>
    </form>

    <div class="row justify-content-center m-3">
        @isset($poll) 
            <div class="col-md-2 col-xs-6">
                <a href="/archivePoll/{{ $poll->id }}"><input type='button' value='Archive Poll'/></a>
            </div>
        @endisset
        <div class="col-md-2 col-xs-6">
            <a href="/home"><input type='button' value='Go Back'/></a>
        </div>
    </div>

</div>
@endsection

@section('scripts')

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script type="text/javascript">

    var count = 1;

    function add() {
        $(poll_selection_add).append('<div class="row justify-content-center"><div class="col-sm-5"><input type="text" id="poll_choices[]" name="poll_choices[]" placeholder="Enter response" class="border-bottom-input"></div></div>');
    }

    function check_errors(event) {

        var num_polldistricts = $('input[name="poll_districts[]"]').length;
         
        if (num_polldistricts < 1) {

            event.preventDefault();
            Swal.fire({
                icon: 'error',
                title: 'No Districts Selected',
                text: 'You must select at least one district',
            })
        }

        var num_pollchoices = $('input[name="poll_choices[]"]').length;

        if (num_pollchoices < 1) {

            event.preventDefault();
            Swal.fire({
                icon: 'error',
                title: 'No Poll Choices',
                text: 'You must create four poll choices',
            })
        }

    }

    function add_districts() {

        //Find out the value the user selected from District Type
        $district = decodeURI($('#district_type :selected').val());  // The value of the selected option

        switch($district) {
            case 'city':
                $district = 'City';
                break;
            case 'boroughs':
                $district = 'Borough';
                break;
            case 'assemblyDistrict':
                $district = 'Assembly';
                break;
            case 'citycouncilDistrict':
                $district = 'City Council';
                break;
            case 'communityDistrict':
                $district = 'Community';
                break;
            case 'congressDistrict':
                $district = 'Congress';
                break;
            case 'stateSenatorialDistrict':
                $district = 'State Senator';
                break;
            default:
                // code block
        }     
        //Find out the value the user selected from District Type
        $district_num = decodeURI($('#district_num :selected').val());  // The value of the selected option\

        //Replace the + with a space like New York City
        $district_num = $district_num.replace(/\+/g, ' ');

        $(show_districts).append('<div class="row" id="' + count + '"><div class="col-md-10 col-xs-8"><input type="text" class="district_label" id="poll_districts[]" name="poll_districts[]" value="' +$district + ':' + $district_num + '" readonly="readonly"></input></div><span class="col-md-2 col-xs-4 fas fa-minus-circle center remove_district" onclick="remove_district(' + count + ')"></span></div>');
        
        count++;
    }

    //Remove the District Option
    function remove_district(item_num) {
        $('#' + item_num).remove()
    }


    //When the user changes the District Type
    function type_changed(sel) {

        //Figure out what the user selected from the Dropdown
        $value = sel.options[sel.selectedIndex].value;

        //Clear the Options from the Dropdown
        $('#district_num').html("");

        //Re-Add the Option Values based on what the user selected
        if ($value === 'city') {
            @foreach ($city as $district)
                var $district = encodeURI('{{ $district }}');
                $('#district_num').append("<option value=" + $district + ">" + '{{ $district }}' + "</option>");
            @endforeach
        }
        else if ($value === 'boroughs') {
            @foreach ($borough as $district)
                var $district = encodeURI('{{ $district }}');
                $('#district_num').append("<option value=" + $district + ">" + '{{ $district }}' + "</option>");
            @endforeach
        }
        else if ($value === 'assemblyDistrict') {
            @foreach ($assembly as $district)
                $('#district_num').append("<option value=" + '{{ $district }}' + ">" + '{{ $district }}' + "</option>");
            @endforeach
        } else if ($value === 'citycouncilDistrict') {
            @foreach ($citycouncil as $district)
                $('#district_num').append("<option value=" + '{{ $district }}' + ">" + '{{ $district }}' + "</option>");
            @endforeach
        } else if ($value === 'stateSenatorialDistrict') {
            @foreach ($senate as $district)
                $('#district_num').append("<option value=" + '{{ $district }}' + ">" + '{{ $district }}' + "</option>");            
            @endforeach
        } else if ($value === 'congressDistrict') {
            @foreach ($congress as $district)
                $('#district_num').append("<option value=" + '{{ $district }}' + ">" + '{{ $district }}' + "</option>");            
            @endforeach
        } else {
            @foreach ($community as $district)
                $('#district_num').append("<option value=" + '{{ $district }}' + ">" + '{{ $district }}' + "</option>");
            @endforeach
        }

        //Find out the value the user selected from District Type
        $district_num = $('#district_num :selected').val();  // The value of the selected option

        $district_num = encodeURI($district_num);

        if ($value === 'city') {
            $link_abbrev = 'city';
        }
        else if ($value === 'boroughs') {
            $link_abbrev = 'b';
        }
        else if ($value === 'assemblyDistrict') {
            $link_abbrev = 'a';    
        } else if ($value === 'citycouncilDistrict') {
            $link_abbrev = 'cc';    
        } else if ($value === 'stateSenatorialDistrict') {
            $link_abbrev = 'ss';    
        } else if ($value === 'congressDistrict') {
            $link_abbrev = 'c';    
        } else {
            $link_abbrev = 'cm';
        }    

    }

    //When the user changes the District Type
    function districtnum_changed (sel) {

        //Find out the value the user selected from District Type
        $district_type = $('#district_type :selected').val();  // The value of the selected option

        //Figure out what the user selected from the Dropdown
        $district_num = sel.options[sel.selectedIndex].value;

        $district_num = encodeURI($district_num);

        if ($district_type === 'city') {
            $link_abbrev = 'city';
        }
        else if ($district_type === 'boroughs') {
            $link_abbrev = 'b';
        }
        else if ($district_type === 'assemblyDistrict') {
            $link_abbrev = 'a';    
        } else if ($district_type === 'citycouncilDistrict') {
            $link_abbrev = 'cc';    
        } else if ($district_type === 'stateSenatorialDistrict') {
            $link_abbrev = 'ss';    
        } else if ($district_type === 'congressDistrict') {
            $link_abbrev = 'c';    
        } else {
            $link_abbrev = 'cm';
        }    
    
    }
</script>


@endsection

