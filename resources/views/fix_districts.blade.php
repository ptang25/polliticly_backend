@extends('layouts.app')

@section('content')
<div class="wrapper">
    <h4 class="centertext">Fix Districts</h4>
    <form method="POST" name="fix_districts" enctype="multipart/form-data" id="fix_districts" action="javascript:void(0)"> 
        @csrf

            <div class="row justify-content-center">
                <label for="date">Voter to fix: </label>
            </div>
            <div class="row justify-content-center">   
                <select id="voter_id" name="voter_id" class="border-bottom-input selectComponent center" onChange="voter_selected(this);">
                    @foreach ($voters as $voter)
                        <option value='{{$voter->id}}'>{{ $voter->first_name . " " . $voter->last_name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="row justify-content-center">
                <label for="house_num">Voter House Number: </label>
            </div>
            <div class="row justify-content-center centerMobile">   
                <input type="text" id="house_num" name="house_num" class="optionsInput" value='{{$voters[0]->house_num}}' class="border-bottom-input">
            </div>

            <div class="row justify-content-center">
                <label for="st_name">Voter Street Name:</label>
            </div>
            <div class="row justify-content-center centerMobile">   
                <input type="text" id="st_name" name="st_name" class="optionsInput" value='{{$voters[0]->street}}' class="border-bottom-input">
            </div>

            <div class="row justify-content-center">
                <label for="zipCode">Voter ZipCode:</label>
            </div>
            <div class="row justify-content-center centerMobile">   
                <input type="text" id="zipCode" name="zipCode" class="optionsInput" value='{{$voters[0]->zipCode}}' class="border-bottom-input">
            </div>

        <div class="row justify-content-center">
            <input type="submit" value="Fix Voter District">
        </div>
    </form>

    <div class="row justify-content-center m-3">
        <a href="/home"><input type='button' value='Go Back'/></a>
    </div>

</div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script type="text/javascript">

    //Send the confirmation Popup Message
    $(document).ready(function (e) {
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#fix_districts').submit(function(e) {
            e.preventDefault();

            var formData = new FormData(this);

            $.ajax({
                type:'POST',
                url: "{{ url('fixVoterDistricts')}}",
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                success: (data) => {

                    Swal.fire({
                        title: 'Results of Fix',
                        text: data,
                        icon: 'info',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Ok'
                        }).then((result) => {
                        if (result.value) {
                            location.reload(); 
                        }
                    })
                    
                },
                error: function(data){
                    console.log(data);
                }
            });
        });
    });    

    //Change Voter Info
    function voter_selected (sel) {

        //Figure out what the user selected from the Dropdown
        $voter_id = sel.options[sel.selectedIndex].value;

        var voters = {!! $voters->toJson() !!};

        for (i = 0; i < voters.length; i++) {

            if (voters[i]['id'] == $voter_id) {

                $('#house_num').val(voters[i]['house_num'])
                $('#st_name').val(voters[i]['street'])
                $('#zipCode').val(voters[i]['zipCode'])
            }
        }


    }
    
    </script>


@endsection

