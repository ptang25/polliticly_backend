@extends('layouts.app')

@section('content')

<div class="row justify-content-center mb-2">
    <label for="date">Comments on Polls</label>
</div>

<table class="blueTable">
    <thead>
        <tr>
            <th>Author</th>
            <th>Comment</th>
            <th>Date</th>
            <th></th>
        </tr>
    </thead>

    <tbody>
            @isset($comments)
                @foreach ($comments as $comment)
                <tr>
                    <td>{{ $comment->username }}</td>
                    <td>{{ $comment->comment }}</td>
                    <td>{{ $comment->created_at }}</td>
                    <td>
                        <i class="fas fa-trash-alt fa-lg" onclick="deletebutton( {{ $comment->id }}, {{ $pollID }})"></i>
                    </td>
                </tr>
                @endforeach
            @endisset
    </tbody>
</table>

<div class="row justify-content-center mb-2">
    <label for="date">Deleted Comments from Polls</label>
</div>

<table class="blueTable">
    <thead>
        <tr>
            <th>Author</th>
            <th>Comment</th>
            <th>Date</th>
            <th></th>
        </tr>
    </thead>

    <tbody>
            @isset($deleted_comments)
                @foreach ($deleted_comments as $comment)
                <tr>
                    <td>{{ $comment->username }}</td>
                    <td>{{ $comment->comment }}</td>
                    <td>{{ $comment->created_at }}</td>
                    <td>
                        <i class="fas fa-trash-alt fa-lg" onclick="deletebutton( {{ $comment->id }})"></i>
                    </td>
                </tr>
                @endforeach
            @endisset
    </tbody>
</table>

@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<script type="text/javascript">

function deletebutton(id, pollID) {

    Swal.fire({
        title: 'Delete Representative',
        text: "Are you sure you want to delete this Comment?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {

                //Delete the poll and reload this page
                window.location.href = '/hideComment/' + id + '/' + pollID;
            }
    })

}

</script>


@endsection

