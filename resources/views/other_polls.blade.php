@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h3 class="centertext"> {{ $poll_type }} </h3>
                </div>
            </div>
        </div>
    </div>

    @isset($polls)
        <div class="row justify-content-center">
            @foreach ($polls as $i => $poll)
                
                    <div class="col-md-4 col-xs-12">
                        <div class="card" style="width: 21rem;">
                            <div class="card-body">
                                <h6 class="card-title centertext">
                                    Poll #{{ $i + 1 }}
                                </h6>
                              <h5 class="card-text centertext">
                                    {{ $poll->question }}
                              </h5>
                              <div class="holder">
                                 @foreach ($poll->choices as $choice) 
                                    <div class="bar justify-content-center"><span class="label">{{ $choice }}</span></div>
                                 @endforeach 
                              </div>
                              
                              <div class="row justify-content-center">
                                    @isset($poll_type)
                                        @if ($poll_type == "Polls waiting for approval")
                                            <div class="mr-2"><a href="/acceptPoll/{{ $poll->id }}" class="btn btn-primary">Approve</a></div>
                                            <a href="/rejectPoll/{{ $poll->id }}" class="btn btn-danger">Reject</a>
                                        @endif
                                    @endisset
                              </div> 

                            </div>
                        </div>
                    </div>
                
            @endforeach
        </div>
    @endisset
    
    <div class="row justify-content-center m-3">
        <a href="/home"><input type='button' value='Go Back'/></a>
    </div>
    
</div>
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script type="text/javascript"> 

    </script>
@endsection
