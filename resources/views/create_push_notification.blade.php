@extends('layouts.app')

@section('content')
<div class="wrapper">
    <h4 class="centertext">Send Push Notifications</h4>
    <form method="POST" name="districts" enctype="multipart/form-data" id="notification_form" action="javascript:void(0)"> 
        @csrf

        <div class="row justify-content-center">
            <label for="name">Message Title:</label>
        </div>
        <div class="row justify-content-center centerMobile">   
            <input type="text" id="message_title" name="message_title" class="optionsInput" class="border-bottom-input">
        </div>

        <div class="row justify-content-center">
            <label for="email">Message Text:</label>
        </div>
        <div class="row justify-content-center centerMobile">   
            <input type="text" id="message" name="message" class="optionsInput" class="border-bottom-input">
        </div>

        <div class="row justify-content-center">
            <label for="date">Target Voters in Districts</label>
        </div>

        <div class="row justify-content-center">   
            
            <div class="col-md-2 col-xs-5 ml-2">
                <select id="district_type" name="district_type" class="selectComponentReg center" onChange="type_changed(this);">
                        <option value='all'>All</option>
                        <option value='assemblyDistrict'>Assembly</option>
                        <option value='citycouncilDistrict'>City Council</option>
                        <option value='communityDistrict'>Community</option>
                        <option value='congressDistrict'>Congress</option>
                        <option value='stateSenatorialDistrict'>Senate</option>
                </select>
            </div>

            <div class="col-md-1 col-xs-5">
                <select id="district_num" name="district_num" class="selectComponentReg center" onChange="districtnum_changed(this);">
                        <option value="all">-</option>
                </select>
            </div>
        </div>
        
        <div class="row justify-content-center">
            <div class="col-md-2 col-xs-8">
                <label>Number of Voters affected:</label>
            </div>
            <div class="col-md-1 col-xs-4">
                <label id="num_voters">{{ $total }}</label>
            </div>
        </div>


        <div class="row justify-content-center">
            <input type="submit" value="Send Notification">
        </div>
    </form>

    <div class="row justify-content-center m-3">
        <a href="/home"><input type='button' value='Go Back'/></a>
    </div>

</div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script type="text/javascript">

    //Send the confirmation Popup Message
    $(document).ready(function (e) {
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#notification_form').submit(function(e) {
            e.preventDefault();

            var formData = new FormData(this);

            $.ajax({
                type:'POST',
                url: "{{ url('sendPN')}}",
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                success: (data) => {
                    Swal.fire({
                        title: 'Success',
                        text: "Notifications successfully sent!",
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Ok'
                        }).then((result) => {
                        if (result.value) {
                            window.location.href = '/home';
                        }
                    })
                },
                error: function(data){
                    console.log(data);
                }
            });
        });
    });    

    //When the user changes the District Type
    function type_changed(sel) {

        //Figure out what the user selected from the Dropdown
        $value = sel.options[sel.selectedIndex].value;

        //Clear the Options from the Dropdown
        $('#district_num').html("");

        //Re-Add the Option Values based on what the user selected
        if ($value === 'all') {
            $('#district_num').append("<option value=" + 'all' + ">" + '-' + "</option>");
            
            //Remove existing label text
            $('#num_voters').html("");

            //Add the new Label text
            $('#num_voters').append(' {{ $total }}');
        } else if ($value === 'assemblyDistrict') {
            @foreach ($assembly as $district)
                $('#district_num').append("<option value=" + '{{ $district }}' + ">" + '{{ $district }}' + "</option>");
            @endforeach
        } else if ($value === 'citycouncilDistrict') {
            @foreach ($citycouncil as $district)
                $('#district_num').append("<option value=" + '{{ $district }}' + ">" + '{{ $district }}' + "</option>");
            @endforeach
        } else if ($value === 'stateSenatorialDistrict') {
            @foreach ($senate as $district)
                $('#district_num').append("<option value=" + '{{ $district }}' + ">" + '{{ $district }}' + "</option>");            
            @endforeach
        } else if ($value === 'congressDistrict') {
            @foreach ($congress as $district)
                $('#district_num').append("<option value=" + '{{ $district }}' + ">" + '{{ $district }}' + "</option>");            
            @endforeach
        } else {
            @foreach ($community as $district)
                $('#district_num').append("<option value=" + '{{ $district }}' + ">" + '{{ $district }}' + "</option>");
            @endforeach
        }
    }

    //When the user changes the District Type
    function districtnum_changed (sel) {

        //Find out the value the user selected from District Type
        $district_type = $('#district_type :selected').val();  // The value of the selected option

        //Figure out what the user selected from the Dropdown
        $district_num = sel.options[sel.selectedIndex].value;

        //Get the index
        $index = $district_type + "_" + $district_num;

        //Transform Array into Javascript variable
        $stats = {!! json_encode($district_stats) !!};

        //Remove existing label text
        $('#num_voters').html("");

        //Add the new Label text - 0 if there is no record
        $('#num_voters').append($stats[$index] ?? 0);
           
    }
    </script>


@endsection

