@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header')
            <!-- header here -->
            <a href="https://www.polliticly.com"><img src="{{ asset('https://dev.polliticly.com/storage/logo.jpg') }}" alt="Polliticly" width="50%" height="90rem"></a>
        @endcomponent
    @endslot

    {{-- Body --}}
    <div class="regularTextFormatting">{{ $email }} has been successfully unsubscribed</div>

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            <!-- footer here -->
        @endcomponent
    @endslot
@endcomponent