@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header')
            <!-- header here -->
            <a href="https://www.polliticly.com"><img src="{{ asset('https://dev.polliticly.com/storage/logo.jpg') }}" alt="Polliticly" width="50%" height="90rem"></a>
        @endcomponent
    @endslot

        <div class="regularTextFormatting">{{ $message }}</div>

{{-- Body --}}
@component('mail::button', ['url' => 'https://polliticly.page.link/open', 'color' => 'polliticly'])
Open in App
@endcomponent

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            To stop recieving emails from us, you can unsubscribe <a href='{{ env('APP_URL') }}/unsubscribe/{{ $email }}'>here</a>
            <!-- footer here -->
        @endcomponent
    @endslot
@endcomponent