@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header')
            <!-- header here -->
            <a href="https://www.polliticly.com"><img src="{{ asset('https://dev.polliticly.com/storage/logo.jpg') }}" alt="Polliticly" width="50%" height="90rem"></a>
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $message }}

    This is to confirm that the above message has been sent to these Elected Officials in your district: {{ $elected_officials_types }}.

    Thank you for using Polliticly.

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            <!-- footer here -->
        @endcomponent
    @endslot
@endcomponent