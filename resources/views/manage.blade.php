@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h3 class="centertext"> Manage Polls </h3>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-4 col-xs-12">
            <div class="card" style="width: 21rem;">
                <div class="card-body">
                  <i class="fas fa-poll-h fa-6x center" alt="Create Poll"></i>
                    <h5 class="card-title centertext">
                        Create Poll
                    </h5>
                  <p class="card-text centertext">
                        Create a new Poll
                  </p>
                  <div class="centerbutton">
                    <a href="/createPoll" class="btn btn-primary">Click Here</a>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <h4 class="centertext"> Polls waiting to be Approved </h4>
    </div>

    <div class="row justify-content-center">            
        <div class="col-md-4 col-xs-12">
            <div class="card" style="width: 21rem;">
                <div class="card-body">
                    <div class="centerbutton">
                        <a href="/approvePolls" class="btn btn-primary">View Polls</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <h4 class="centertext"> Active Polls </h4>
    </div>

    @isset($polls)
        <div class="row justify-content-center">
            @foreach ($polls as $i => $poll)
                
                    <div class="col-md-4 col-xs-12">
                        <div class="card" style="width: 21rem;">
                            <div class="card-body">
                                <h6 class="card-title centertext">
                                    Poll #{{ $i + 1 }}
                                </h6>
                              <h5 class="card-text centertext">
                                    {{ $poll->question }}
                              </h5>
                              <div class="holder">
                                 @for ($x = 0; $x < count($poll->percentages); $x++) 
                                    <div class="bar" data-percent= {{ $poll->percentages[$x] * 100 }} display-percent = {{ ($poll->percentages[$x] * 100 > 80) ? 100 : ($poll->percentages[$x] * 100 + 20) }}><span class="label">{{ $poll->choices[$x] }}</span></div>
                                 @endfor 
                              </div>
                              
                              <div class="row">
                                  <div class="col">
                                        <a href="/updatePoll/{{ $poll->id }}" class="btn btn-primary">Edit</a>
                                        <div class="btn btn-danger" onclick="deletebutton( {{ $poll->id }})">Delete</div>
                                        <a href="/commentsPage/{{ $poll->id }}" class="btn btn-info">Comments</a>
                                        <a href="/resultsPages/{{ $poll->id }}" class="btn btn-success">Generate</a>
                                  </div>
                                
                              </div> 
                            </div>
                        </div>
                    </div>
                
            @endforeach
        </div>
    @endisset

    <div class="row justify-content-center">
        <div class="col">
            <h4 class="centertext"> Inactive Polls </h4>
        </div>

    <div class="row justify-content-center">            
        <div class="col-md-4 col-xs-12">
            <div class="card" style="width: 21rem;">
                <div class="card-body">
                    <div class="centerbutton">
                        <a href="/archivedPolls" class="btn btn-primary">View Archived Polls</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <h4 class="centertext"> Disapproved Polls </h4>
    </div>

    <div class="row justify-content-center">            
        <div class="col-md-4 col-xs-12">
            <div class="card" style="width: 21rem;">
                <div class="card-body">
                    <div class="centerbutton">
                        <a href="/rejectedPolls" class="btn btn-primary">View Polls</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script type="text/javascript"> 

        function deletebutton(id) {

            Swal.fire({
                title: 'Delete Poll',
                text: "Are you sure you want to delete this Poll?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
                }).then((result) => {
                    if (result.value) {

                        //Delete the poll and reload this page
                        window.location.href = '/deletePoll/' + id;
                    }
            })
            
        }

        /** Code By Webdevtrick ( https://webdevtrick.com ) **/
        setTimeout(function start (){
        
        $('.bar').each(function(i){  
            var $bar = $(this);
            $(this).append('<span class="count"></span>')
            setTimeout(function(){
            $bar.css('width', $bar.attr('display-percent') + "%");      
            }, i*100);
        });
        
        $('.count').each(function () {
            $(this).prop('Counter',0).animate({
                Counter: $(this).parent('.bar').attr('data-percent')
            }, {
                duration: 2000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now) +'%');
                }
            });
        });

        }, 500)

        

    </script>
@endsection
