@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Welcome {{ Auth::user()->name }}
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-4 col-xs-12">
            <div class="card" style="width: 21rem;">
                <div class="card-body">
                  <i class="fas fa-poll fa-6x center" alt="Create Poll"></i>
                    <h5 class="card-title centertext">
                        Manage Polls
                    </h5>
                  <p class="card-text centertext">
                        Create, Hide, Edit Polls
                  </p>
                  <div class="centerbutton">
                    <a href="/managePolls" class="btn btn-primary">Click Here</a>
                  </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="card" style="width: 21rem;">
                <div class="card-body">
                  <i class="fas fa-poll fa-6x center" alt="Create Poll"></i>
                    <h5 class="card-title centertext">
                        Manage Representatives
                    </h5>
                  <p class="card-text centertext">
                        Edit and Create District Representatives
                  </p>
                  <div class="centerbutton">
                    <a href="/showDistricts" class="btn btn-primary">Click Here</a>
                  </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="card" style="width: 21rem;">
                <div class="card-body">
                  <i class="fas fa-bell fa-6x center" alt="Create Poll"></i>
                    <h5 class="card-title centertext">
                        Send Push Notifications
                    </h5>
                  <p class="card-text centertext">
                        Target Voters to Send Push Notifications
                  </p>
                  <div class="centerbutton">
                    <a href="/createPN" class="btn btn-primary">Click Here</a>
                  </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
          <div class="card" style="width: 21rem;">
              <div class="card-body">
                <i class="fas fa-bell fa-6x center" alt="Create Poll"></i>
                  <h5 class="card-title centertext">
                      Fix Voter Districts
                  </h5>
                <p class="card-text centertext">
                      Fix Address Input Errors
                </p>
                <div class="centerbutton">
                  <a href="/fixVoters" class="btn btn-primary">Click Here</a>
                </div>
              </div>
          </div>
      </div>

    </div>
</div>
@endsection
