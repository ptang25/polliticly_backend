<tr>
<td>
<table class="footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td class="content-cell" align="center">
{{ Illuminate\Mail\Markdown::parse($slot) }}
<p>This email was generated using the <a href="https://polliticly.com/">Polliticly</a> App. </p> 
<p>Bridging the communication gap between you and your elected officials.</p>
<div id="copyright text-right">© Copyright {{ date('Y') }} Polliticly</div>
</td>
</tr>
</table>
</td>
</tr>
