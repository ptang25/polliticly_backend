@extends('layouts.app')

@section('content')
<div class="wrapper">
    <h4 class="centertext">Add a Representative</h4>
    <form method="POST" name="representative" enctype="multipart/form-data" id="matchup_form" onsubmit="return validateForm()" action="/storeRep"> 
        @csrf

        @isset($member)
            <input type="hidden" id="repID" name="repID" value="{{ $member->id }}">
        @endisset 

        <div class="row justify-content-center">
            <label for="date">Type of Representative</label>
        </div>
        <div class="row justify-content-center">   
            <select id="type" name="type" class="border-bottom-input selectComponent center">
                    <option value='assemblyDistrict'
                        @isset($member)
                            @if ($member->district_type == 'assemblyDistrict')
                                selected
                            @endif
                        @endisset
                    >Assembly</option>
                    <option value='citycouncilDistrict'
                        @isset($member)
                            @if ($member->district_type == 'cityCouncilDistrict')
                                selected
                            @endif
                        @endisset
                    >City Council</option>
                    <option value='communityDistrict'
                        @isset($member)
                            @if ($member->district_type == 'communityDistrict')
                                selected
                            @endif
                        @endisset
                    >Community</option>
                    <option value='congressDistrict'
                        @isset($member)
                            @if ($member->district_type == 'congressionalDistrict')
                                selected
                            @endif
                        @endisset
                    >Congress</option>
                    <option value='stateSenatorialDistrict'
                        @isset($member)
                            @if ($member->district_type == 'stateSenatorialDistrict')
                                selected
                            @endif
                        @endisset
                    >Senate</option>
            </select>
        </div>
        <div class="row justify-content-center">
            <label for="district_num">District Number:</label>
        </div>
        <div class="row justify-content-center centerMobile">   
            <input type="text" id="district_num" name="district_num" class="optionsInput" 
                @isset($member)
                    value='{{$member->district_num}}' 
                @endisset
                    class="border-bottom-input">
        </div>
        <div class="row justify-content-center">
            <label for="name">Representative Name:</label>
        </div>
        <div class="row justify-content-center centerMobile">   
            <input type="text" id="name" name="name" class="optionsInput"
                @isset($member)
                    value='{{$member->rep_name}}' 
                @endisset
                    class="border-bottom-input">
        </div>

        <div class="row justify-content-center">
            <label for="email">Representative Email:</label>
        </div>
        <div class="row justify-content-center centerMobile">   
            <input type="text" id="email" name="email" class="optionsInput"
                @isset($member)
                    value='{{$member->rep_email}}' 
                @endisset
                    class="border-bottom-input">
        </div>

        <div class="row justify-content-center">
            <label for="details">Representative Notes:</label>
        </div>
        <div class="row justify-content-center centerMobile">   
            <input type="text" id="details" name="details" class="optionsInput"
                @isset($member)
                    value='{{$member->rep_details}}' 
                @endisset
                    class="border-bottom-input">
        </div>

        <div class="row justify-content-center">
            <input type="submit" value=
                @isset($member)
                    "Update Representative"
                @else
                    "Add Representative"
                @endisset 
            >
        </div>
    </form>

    <div class="row justify-content-center m-3">
        <a href="/home"><input type='button' value='Go Back'/></a>
    </div>

</div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script type="text/javascript">

    /* Validate that the District Number only contains Digits */    
    function validateForm() {

        $value = document.representative.district_num.value;

        //Return False with a Prompt if the District Number contains anything other than Digits
        if ($value.match(/^[0-9]+$/) === null) {

            Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'District Number must be only contain numbers',
            })

            return false;
        } 
        else {
            return true;
        }
    }  
    
    </script>


@endsection

