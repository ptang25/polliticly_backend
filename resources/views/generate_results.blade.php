@extends('layouts.app')

@section('content')
<div class="wrapper">
    <h4 class="centertext">Generate Results Page</h4>
   
    @isset($poll)
        <input type="hidden" id="pollID" name="pollID" value="{{ $poll->id }}">
    @endisset 

    <div class="row justify-content-center">
        <label for="date">District</label>
    </div>

    <div class="row justify-content-center">   
            
        <div class="col-md-2 col-xs-5 ml-2">
            <select id="district_type" name="district_type" class="selectComponentReg center" onChange="type_changed(this);">
                    <option value='city'>City</option>
                    <option value='boroughs'>Boroughs</option>
                    <option value='assemblyDistrict' selected='true'>Assembly</option>
                    <option value='citycouncilDistrict'>City Council</option>
                    <option value='communityDistrict'>Community</option>
                    <option value='congressDistrict'>Congress</option>
                    <option value='stateSenatorialDistrict'>Senate</option>
            </select>
        </div>

        <div class="col-md-2 col-xs-5">
            <select id="district_num" name="district_num" class="selectComponentReg center" onChange="districtnum_changed(this);">
                @foreach ($assembly as $district)
                    <option value='{{ $district }}'>{{ $district }}</option>
                @endforeach
            </select>
        </div>
    </div>
        
    <div class="row justify-content-center">
            <h4 class="generated_pages_title">Generated URL</h4>
    </div>

    <div class="row justify-content-center mb-2">
        <h4 class="results_border">
            <div id="results_page_link" class="link_style">{{ env('APP_URL') }}/poll_results/{{ $poll->hashcode }}/a/{{ $assembly[0] }}</div>
        </h4>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-1 col-xs-4 center_buttons" onclick="copyToClipboard('#results_page_link')">
            <i class="far fa-copy fa-2x"></i>
        </div>
        <div class="col-md-1 col-xs-4">
            <i class="fas fa-external-link-alt fa-2x" onclick="openLink()"></i>
        </div>
    </div>

    <div class="row justify-content-center m-3">
        <a href="/home"><input type='button' value='Go Back'/></a>
    </div>

</div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script type="text/javascript">

    //When the user changes the District Type
    function type_changed(sel) {

        //Figure out what the user selected from the Dropdown
        $value = sel.options[sel.selectedIndex].value;

        //Clear the Options from the Dropdown
        $('#district_num').html("");

        //Re-Add the Option Values based on what the user selected
        if ($value === 'city') {
            @foreach ($city as $district)
                var $district = encodeURI('{{ $district }}');
                $('#district_num').append("<option value=" + $district + ">" + '{{ $district }}' + "</option>");
            @endforeach
        }
        else if ($value === 'boroughs') {
            @foreach ($borough as $district)
                var $district = encodeURI('{{ $district }}');
                $('#district_num').append("<option value=" + $district + ">" + '{{ $district }}' + "</option>");
            @endforeach
        }
        else if ($value === 'assemblyDistrict') {
            @foreach ($assembly as $district)
                $('#district_num').append("<option value=" + '{{ $district }}' + ">" + '{{ $district }}' + "</option>");
            @endforeach
        } else if ($value === 'citycouncilDistrict') {
            @foreach ($citycouncil as $district)
                $('#district_num').append("<option value=" + '{{ $district }}' + ">" + '{{ $district }}' + "</option>");
            @endforeach
        } else if ($value === 'stateSenatorialDistrict') {
            @foreach ($senate as $district)
                $('#district_num').append("<option value=" + '{{ $district }}' + ">" + '{{ $district }}' + "</option>");            
            @endforeach
        } else if ($value === 'congressDistrict') {
            @foreach ($congress as $district)
                $('#district_num').append("<option value=" + '{{ $district }}' + ">" + '{{ $district }}' + "</option>");            
            @endforeach
        } else {
            @foreach ($community as $district)
                $('#district_num').append("<option value=" + '{{ $district }}' + ">" + '{{ $district }}' + "</option>");
            @endforeach
        }

        //Find out the value the user selected from District Type
        $district_num = $('#district_num :selected').val();  // The value of the selected option

        $district_num = encodeURI($district_num);

        if ($value === 'city') {
            $link_abbrev = 'city';
        }
        else if ($value === 'boroughs') {
            $link_abbrev = 'b';
        }
        else if ($value === 'assemblyDistrict') {
            $link_abbrev = 'a';    
        } else if ($value === 'citycouncilDistrict') {
            $link_abbrev = 'cc';    
        } else if ($value === 'stateSenatorialDistrict') {
            $link_abbrev = 'ss';    
        } else if ($value === 'congressDistrict') {
            $link_abbrev = 'c';    
        } else {
            $link_abbrev = 'cm';
        }    

        //Set the Link
        $index = "{{ env('APP_URL') }}" + "/poll_results/" + '{{ $poll->hashcode }}' + "/" + $link_abbrev + "/" + $district_num;

        //Remove existing label text
        $('#results_page_link').html("");

        //Add the new Label text - 0 if there is no record
        $('#results_page_link').append($index);

    }

    //When the user changes the District Type
    function districtnum_changed (sel) {

        //Find out the value the user selected from District Type
        $district_type = $('#district_type :selected').val();  // The value of the selected option

        //Figure out what the user selected from the Dropdown
        $district_num = sel.options[sel.selectedIndex].value;

        $district_num = encodeURI($district_num);

        if ($district_type === 'city') {
            $link_abbrev = 'city';
        }
        else if ($district_type === 'boroughs') {
            $link_abbrev = 'b';
        }
        else if ($district_type === 'assemblyDistrict') {
            $link_abbrev = 'a';    
        } else if ($district_type === 'citycouncilDistrict') {
            $link_abbrev = 'cc';    
        } else if ($district_type === 'stateSenatorialDistrict') {
            $link_abbrev = 'ss';    
        } else if ($district_type === 'congressDistrict') {
            $link_abbrev = 'c';    
        } else {
            $link_abbrev = 'cm';
        }    

        //Set the Link
        $index = "{{ env('APP_URL') }}" + "/poll_results/" + '{{ $poll->hashcode }}' + "/" + $link_abbrev + "/" + $district_num;

        //Remove existing label text
        $('#results_page_link').html("");

        //Add the new Label text - 0 if there is no record
        $('#results_page_link').append($index);
           
    }

    /* Copy to Clipboard - we create a fake field, add the contents to there, and then use execCommand to copy to Clipboard */
    function copyToClipboard(element) {

        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        $temp.remove();

        //Show Popup
        Swal.fire(
            'Copied!',
            'Link copied to Clipboard!',
            'success'
        )
    }

    function openLink() {

        //Get Link from field
        $link = $('#results_page_link').text();

        //Open new Tab
        window.open($link, '_blank');
    }

    </script>


@endsection

