@extends('layouts.app')

@section('content')

    

    <div class="limiter">
        <div class="container-table100">

            <div class="row justify-content-center">
                <div class="col-md-5 col-xs-8">
                    <div class="box mb-2">
                        <select id="select_reps" onChange="rep_changed(this);">
                          <option value="assembly">Assembly</option>
                          <option value="citycouncil">City Council</option>
                          <option value="community">Community</option>
                          <option value="congress">Congress</option>
                          <option value="senate">Senate</option>
                        </select>
                      </div>
                </div>
                <div class="col-md-1 col-xs-3">
                    <a href="addRep"><div class="fas fa-plus-circle fa-2x center faCenter"></div></a>
                </div>
            </div>


            <div class="wrap-table100">
                    <div class="table tableRep">
                        <div class="row rowRep header">
                            <div class="cell">
                                District #
                            </div>
                            <div class="cell">
                                Name
                            </div>
                            <div class="cell">
                                Email
                            </div>
                            <div class="cell">
                                Actions
                            </div>
                        </div>

                        <div class="row" id="reps">
                            @isset($assembly)
                                <div id="repsTable">
                                @foreach ($assembly as $member)
                                    <div class="rowRep">
                                        <div class="cell" data-title="District #">
                                            {{ $member->district_num }}
                                        </div>
                                        <div class="cell" data-title="Name">
                                            {{ $member->rep_name }}
                                        </div>
                                        <div class="cell" data-title="Email">
                                            {{ $member->rep_email }}
                                        </div>
                                        <div class="cell" data-title="Actions">
                                            <div class="col-md-4 col-xs-4">
                                                <a href="editRep/{{ $member->id }}"><i class="fas fa-edit fa-lg"></i></a>
                                            </div>
                                            <div class="col-md-4 col-xs-4">
                                                <i class="fas fa-trash-alt fa-lg" onclick="deletebutton( {{ $member->id }})"></i>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </div>
                            @endisset
                        </div>

                    </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<script type="text/javascript">

function deletebutton(id) {

    Swal.fire({
        title: 'Delete Representative',
        text: "Are you sure you want to delete this Representative?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {

                //Delete the poll and reload this page
                window.location.href = '/removeRep/' + id;
            }
    })

}

function rep_changed(sel) {

    $value = sel.options[sel.selectedIndex].value;

    $(repsTable).remove();

    if ($value === 'assembly') {
        @foreach ($assembly as $member)
            $(reps).append('<div id="repsTable"><div class="rowRep"><div class="cell" data-title="District #">' + '{{ $member->district_num }}' + '</div><div class="cell" data-title="Name">' + '{{ $member->rep_name }}' + '</div><div class="cell" data-title="Email">' + '{{ $member->rep_email }}' + '</div><div class="cell" data-title="Actions">' + '<div class="col-md-4 col-xs-4"><a href="editRep/' + '{{ $member->id }}' + '"><i class="fas fa-edit fa-lg"></i></a></div><div class="col-md-4 col-xs-4"><i class="fas fa-trash-alt fa-lg" onclick="deletebutton( ' + '{{ $member->id }})' + '"></i></div></div>' + '</div></div></div>');
        @endforeach
    } else if ($value === 'citycouncil') {
        @foreach ($citycouncil as $member)
            $(reps).append('<div id="repsTable"><div class="rowRep"><div class="cell" data-title="District #">' + '{{ $member->district_num }}' + '</div><div class="cell" data-title="Name">' + '{{ $member->rep_name }}' + '</div><div class="cell" data-title="Email">' + '{{ $member->rep_email }}' + '</div><div class="cell" data-title="Actions">' + '<div class="col-md-4 col-xs-4"><a href="editRep/' + '{{ $member->id }}' + '"><i class="fas fa-edit fa-lg"></i></a></div><div class="col-md-4 col-xs-4"><i class="fas fa-trash-alt fa-lg" onclick="deletebutton( ' + '{{ $member->id }})' + '"></i></div></div>' + '</div></div></div>');
        @endforeach
    } else if ($value === 'senate') {
        @foreach ($senate as $member)
            $(reps).append('<div id="repsTable"><div class="rowRep"><div class="cell" data-title="District #">' + '{{ $member->district_num }}' + '</div><div class="cell" data-title="Name">' + '{{ $member->rep_name }}' + '</div><div class="cell" data-title="Email">' + '{{ $member->rep_email }}' + '</div><div class="cell" data-title="Actions">' + '<div class="col-md-4 col-xs-4"><a href="editRep/' + '{{ $member->id }}' + '"><i class="fas fa-edit fa-lg"></i></a></div><div class="col-md-4 col-xs-4"><i class="fas fa-trash-alt fa-lg" onclick="deletebutton( ' + '{{ $member->id }})' + '"></i></div></div>' + '</div></div></div>');
        @endforeach
    } else if ($value === 'congress') {
        @foreach ($congress as $member)
            $(reps).append('<div id="repsTable"><div class="rowRep"><div class="cell" data-title="District #">' + '{{ $member->district_num }}' + '</div><div class="cell" data-title="Name">' + '{{ $member->rep_name }}' + '</div><div class="cell" data-title="Email">' + '{{ $member->rep_email }}' + '</div><div class="cell" data-title="Actions">' + '<div class="col-md-4 col-xs-4"><a href="editRep/' + '{{ $member->id }}' + '"><i class="fas fa-edit fa-lg"></i></a></div><div class="col-md-4 col-xs-4"><i class="fas fa-trash-alt fa-lg" onclick="deletebutton( ' + '{{ $member->id }})' + '"></i></div></div>' + '</div></div></div>');
        @endforeach
    } else {
        @foreach ($community as $member)
            $(reps).append('<div id="repsTable"><div class="rowRep"><div class="cell" data-title="District #">' + '{{ $member->district_num }}' + '</div><div class="cell" data-title="Name">' + '{{ $member->rep_name }}' + '</div><div class="cell" data-title="Email">' + '{{ $member->rep_email }}' + '</div><div class="cell" data-title="Actions">' + '<div class="col-md-4 col-xs-4"><a href="editRep/' + '{{ $member->id }}' + '"><i class="fas fa-edit fa-lg"></i></a></div><div class="col-md-4 col-xs-4"><i class="fas fa-trash-alt fa-lg" onclick="deletebutton( ' + '{{ $member->id }})' + '"></i></div></div>' + '</div></div></div>');
        @endforeach
    }
}

</script>


@endsection

