@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if ($district_type == "City")
                        <h3 class="centertext"> Results for {{ urldecode($district_num) }} </h3>
                    @elseif ($district_type == "Boroughs")
                        <h3 class="centertext"> Results for {{ urldecode($district_num) }} Borough </h3>
                    @else
                        <h3 class="centertext"> Results for {{ $district_type }} District {{ $district_num }} </h3>
                    @endif
                    <h5 class="centertext"> Poll Question: {{ $question }} </h5>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div id="poll_div" class="main-graph-dimensions"></div>
        @columnchart('Votes', 'poll_div')
    </div> 

    <hr />

    <!-- Select Box -->
    <div class="row justify-content-center">   
            
        <select id="poll_type" name="poll_type" class="selectComponentReg center" onChange="type_changed(this);">
                    <option value='Age' selected=true>Age</option>
                    <option value='Ethnicity'>Ethnicity</option>
                    <option value='Gender'>Gender</option>
                    <option value='DistrictOnly'>Only District Results</option>
        </select>

    </div>

    <hr />
    
    <!-- Each Graph set is broken into sections, we're using a CSS trick to allow the graph to render, but stay hidden -->
    <div id="age-section">
        <div class="row justify-content-center mb-2"> 
            <div id="combo-div" class="graph-dimensions"></div> 
            @combochart('Age_Selections', 'combo-div')
        </div>
        
        <div class="row justify-content-center"> 
            <div id="pie-div" class="graph-dimensions"></div> 
            @piechart('Age', 'pie-div')
        </div>
    </div>

    <!-- Render the graph, but hide it -->
    <div id="gender-section" class="invisible">
        <div class="row justify-content-center mb-2"> 
            <div id="gender-div" class="graph-dimensions"></div> 
            @combochart('Gender_Selections', 'gender-div')
        </div>

        <div class="row justify-content-center"> 
            <div id="genderpie-div" class="graph-dimensions"></div> 
            @piechart('Gender', 'genderpie-div')
        </div>    
    </div>

    <!-- Render the graph, but hide it -->
    <div id="ethnicity-section" class="invisible">
        <div class="row justify-content-center mb-2"> 
            <div id="ethnicity-div" class="graph-dimensions"></div> 
            @combochart('Ethnicity_Selections', 'ethnicity-div')
        </div>

        <div class="row justify-content-center"> 
            <div id="ethnicitypie-div" class="graph-dimensions"></div> 
            @piechart('Ethnicity', 'ethnicitypie-div')
        </div>
    </div>

    <!-- Render the graph, but hide it -->
    <div id="district-section" class="invisible">
        <div class="row justify-content-center"> 
            <div id="district-div" class="graph-dimensions"></div> 
            @if ($district_type == "City")
                @columnchart('CityVotes', 'district-div')
            @elseif ($district_type == "Boroughs")
                @columnchart('BoroughVotes', 'district-div')
            @else
                @columnchart('DistrictVotes', 'district-div')
            @endif
        </div>
    </div>
    
    <!-- Show Polliticly's Logo -->
    <div class="row justify-content-center">
        <h4>Proudly Powered by</h4>
    </div>
    <div class="row justify-content-center">
        <a href="http://polliticly.com"><img src="{{ URL::asset('storage/logo.jpg') }}" width=35% class="center"></a>
    </div>

</div>
@endsection

@section('scripts')

    <script type="text/javascript">

    //When the user changes the District Type
    function type_changed(sel) {

        //Figure out what the user selected from the Dropdown
        $value = sel.options[sel.selectedIndex].value;

        //Using CSS, we remove the css that makes the component invisible, while setting the other section invisible
        if ($value == "Ethnicity") {
            $('#age-section').addClass("invisible");
            $('#gender-section').addClass("invisible");
            $('#district-section').addClass("invisible");    

            $('#ethnicity-section').removeClass("invisible");           
        } else if ($value == "Gender") {
            $('#age-section').addClass("invisible");
            $('#ethnicity-section').addClass("invisible");   
            $('#district-section').addClass("invisible");    

            $('#gender-section').removeClass("invisible");
        } else if ($value == "Age") {
            $('#gender-section').addClass("invisible");
            $('#ethnicity-section').addClass("invisible"); 
            $('#district-section').addClass("invisible");    
            
            $('#age-section').removeClass("invisible");
        } else if ($value == "DistrictOnly") {
            $('#gender-section').addClass("invisible");
            $('#ethnicity-section').addClass("invisible");     
            $('#age-section').addClass("invisible");

            $('#district-section').removeClass("invisible");
        } else if ($value == "CityOnly") {
            $('#gender-section').addClass("invisible");
            $('#ethnicity-section').addClass("invisible");     
            $('#age-section').addClass("invisible");

            $('#district-section').removeClass("invisible");
        }

    }

    </script>

@endsection


